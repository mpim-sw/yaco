#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023 Max Planck Institute for Meteorology, Yaco authors
#
# SPDX-License-Identifier: BSD-3-Clause

import argparse
from copy import deepcopy
from xml.dom import minidom

import yaml


def new_node(root, name, *args, **attributes):
    node = root.createElement(name)
    for text in args:
        node.appendChild(root.createTextNode(str(text)))
    for k, v in attributes.items():
        node.setAttribute(k, str(v))
    return node


class YACConfig:
    def __init__(self, filename):
        self.components = {}
        self.couples = {}
        self.grids = {}
        self.transients = {}

        config = yaml.load(open(filename, "r"), Loader=yaml.SafeLoader)
        self.coupling_xsd = config.get("xsd", None)
        self.yaco_config = config.get("yaco", None)
        self.dates = config["dates"]

        self.variables = {
            name: {"collection_size": collection_size}
            for name, collection_size in config["variables"].items()
        }

        for name, component in config["components"].items():
            self.components[name] = {
                "id": len(self.components) + 1,
                "model": component.get("model", "none"),
                "simulated": component.get("simulated", "none"),
                "type": component.get("type", "default"),
                "transient_grid_refs": {},
            }

        for coupling in config["couplings"]:
            component1 = self.components[coupling["source"]["component"]]
            component2 = self.components[coupling["target"]["component"]]
            assert component1 != component2
            if component1["id"] > component2["id"]:
                component1, component2 = component2, component1

            k = (component1["id"], component2["id"])
            if k not in self.couples:
                self.couples[k] = []

            for variable, transient_couple in coupling["variables"].items():
                transient_couple["variable"] = variable
                transient_couple["source"] = deepcopy(coupling["source"])
                transient_couple["source"][
                    "transient_grid_ref"
                ] = self.__get_transient_grid_ref(
                    coupling["source"]["component"],
                    variable,
                    coupling["source"]["grid"],
                )[
                    "id"
                ]
                transient_couple["target"] = deepcopy(coupling["target"])
                transient_couple["target"][
                    "transient_grid_ref"
                ] = self.__get_transient_grid_ref(
                    coupling["target"]["component"],
                    variable,
                    coupling["target"]["grid"],
                )[
                    "id"
                ]
                if not transient_couple.get("define_only", False):
                    self.couples[k].append(transient_couple)

    def __get_transient_grid_ref(self, component, variable, grid):
        component = self.components[component]
        if grid not in self.grids:
            self.grids[grid] = {"id": len(self.grids) + 1}

        if variable not in self.transients:
            self.transients[variable] = {
                "id": len(self.transients) + 1,
                "collection_size": self.variables[variable]["collection_size"],
            }

        k = (variable, grid)
        if k not in component["transient_grid_refs"]:
            component["transient_grid_refs"][k] = {
                "id": len(component["transient_grid_refs"]) + 1,
                "variable": variable,
                "grid": grid,
            }

        return component["transient_grid_refs"][k]

    def __get_couples_node(self, root):
        couples = new_node(root, "couples")

        for (component1, component2), transient_couples in self.couples.items():
            couple = new_node(root, "couple")
            couple.appendChild(new_node(root, "component1", component_id=component1))
            couple.appendChild(new_node(root, "component2", component_id=component2))

            for transient_couple in transient_couples:
                node = new_node(
                    root,
                    "transient_couple",
                    transient_id=self.transients[transient_couple["variable"]]["id"],
                )
                node.appendChild(
                    new_node(
                        root,
                        "source",
                        component_ref=1
                        if self.components[transient_couple["source"]["component"]][
                            "id"
                        ]
                        == component1
                        else 2,
                        transient_grid_ref=transient_couple["source"][
                            "transient_grid_ref"
                        ],
                    )
                )
                node.appendChild(
                    new_node(
                        root,
                        "target",
                        transient_grid_ref=transient_couple["target"][
                            "transient_grid_ref"
                        ],
                    )
                )
                timestep = new_node(root, "timestep")
                timestep.appendChild(
                    new_node(root, "source", transient_couple["source"]["timestep"])
                )
                timestep.appendChild(
                    new_node(root, "target", transient_couple["target"]["timestep"])
                )
                timestep.appendChild(
                    new_node(
                        root,
                        "coupling_period",
                        transient_couple["coupling_period"],
                        operation=transient_couple["coupling_period_operation"],
                    )
                )
                timestep.appendChild(
                    new_node(
                        root, "source_timelag", transient_couple["source"]["timelag"]
                    )
                )
                timestep.appendChild(
                    new_node(
                        root, "target_timelag", transient_couple["target"]["timelag"]
                    )
                )
                node.appendChild(timestep)

                node.appendChild(
                    new_node(
                        root, "mapping_on_source", transient_couple["mapping_on_source"]
                    )
                )

                interpolation_requirements = new_node(
                    root, "interpolation_requirements"
                )
                for interpolation in transient_couple["interpolation_requirements"]:
                    interpolation_requirements.appendChild(
                        new_node(root, "interpolation", **interpolation)
                    )
                node.appendChild(interpolation_requirements)

                node.appendChild(
                    new_node(
                        root,
                        "enforce_write_weight_file",
                        transient_couple["enforce_write_weight_file"],
                    )
                )

                couple.appendChild(node)

            couples.appendChild(couple)

        return couples

    def __get_components_node(self, root):
        components = new_node(root, "components")

        for name, component in self.components.items():
            node = new_node(root, "component", id=component["id"])
            node.appendChild(new_node(root, "name", name))
            node.appendChild(new_node(root, "model", component["model"]))
            node.appendChild(new_node(root, "simulated", component["simulated"]))

            transient_grid_refs = new_node(root, "transient_grid_refs")

            for (variable, grid), v in component["transient_grid_refs"].items():
                transient_grid_refs.appendChild(
                    new_node(
                        root,
                        "transient_grid_ref",
                        collection_size=self.transients[variable]["collection_size"],
                        grid_ref=self.grids[grid]["id"],
                        id=v["id"],
                        transient_ref=self.transients[variable]["id"],
                    )
                )

            node.appendChild(transient_grid_refs)
            components.appendChild(node)

        return components

    def __get_grids_node(self, root):
        node = new_node(root, "grids")
        for name, g in self.grids.items():
            grid = new_node(root, "grid", id=g["id"], alias_name=name)
            node.appendChild(grid)
        return node

    def __get_transients_node(self, root):
        node = new_node(root, "transients")
        for name, variable in self.transients.items():
            transient = new_node(
                root, "transient", id=variable["id"], transient_standard_name=name
            )
            node.appendChild(transient)
        return node

    def write_xml(self, filename):
        root = minidom.Document()

        coupling = new_node(
            root,
            "coupling",
            **{
                "xmlns": "http://www.w3schools.com",
                "xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
                "xsi:schemaLocation": "http://www.w3schools.com{}".format(
                    " " + self.coupling_xsd if self.coupling_xsd else ""
                ),
            }
        )

        coupling.appendChild(
            new_node(
                root, "redirect", redirect_of_root="false", redirect_stdout="false"
            )
        )

        coupling.appendChild(self.__get_components_node(root))
        coupling.appendChild(self.__get_transients_node(root))
        coupling.appendChild(self.__get_grids_node(root))

        dates = new_node(root, "dates")
        for v in ["start_date", "end_date", "calendar"]:
            dates.appendChild(new_node(root, v, self.dates[v]))
        coupling.appendChild(dates)

        coupling.appendChild(new_node(root, "timestep_unit", "ISO_format"))

        coupling.appendChild(self.__get_couples_node(root))

        root.appendChild(coupling)

        with open(filename, "wb") as f:
            f.write(root.toprettyxml(indent="  ", encoding="UTF-8"))

    def write_yaco_yaml(self, filename, xml_filename):
        if self.yaco_config is None:
            raise Exception("No yaco config in input file")
        ranks = {}
        rank = int(self.yaco_config["rank_offset"])
        yaco_timestep = None
        yaco_timelag = None
        for k, v in self.components.items():
            if v["type"].upper() == "YACO":
                collections = []
                for _, transient_couples in self.couples.items():
                    for transient_couple in transient_couples:
                        if transient_couple["target"]["component"] != k:
                            continue
                        timestep = transient_couple["target"]["timestep"]
                        timelag = transient_couple["target"]["timelag"]
                        if yaco_timestep is None:
                            yaco_timestep = timestep
                        else:
                            assert yaco_timestep == timestep
                        if yaco_timelag is None:
                            yaco_timelag = timelag
                        else:
                            assert yaco_timelag == timelag
                        var = transient_couple["variable"]
                        collections.append(
                            {
                                "name": var,
                                "size": self.transients[var]["collection_size"],
                            }
                        )
                ranks[rank] = {
                    "name": k,
                    "yac_xml": xml_filename,
                    "yac_xsd": self.coupling_xsd if self.coupling_xsd else "",
                    "collections": collections,
                    "time_step": yaco_timestep,
                    "time_lag": yaco_timelag,
                }
                for i in ["start_date", "end_date", "calendar"]:
                    ranks[rank][i] = self.yaco_config[i]
                rank += 1
        yaml.dump(
            {
                "output": self.yaco_config["output"],
                "grid": self.yaco_config["grid"],
                "fdb_writer": self.yaco_config["fdb_writer"],
                "yac_input": {"ranks": ranks},
            },
            open(filename, "w"),
            default_flow_style=False,
        )


parser = argparse.ArgumentParser(description="")
parser.add_argument("infile", type=str)
parser.add_argument("xmloutput", type=str)
parser.add_argument("--yaco-config", type=str)
args = parser.parse_args()

c = YACConfig(args.infile)
c.write_xml(args.xmloutput)
if args.yaco_config:
    c.write_yaco_yaml(args.yaco_config, args.xmloutput)
