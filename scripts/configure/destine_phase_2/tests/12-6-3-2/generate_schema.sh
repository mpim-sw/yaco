generate_schema(){
    schema_file=$1
    fdb_config=$2
    fdb_path=$3

    cat > ${fdb_config} << EOF
type: local
engine: toc
schema: ${schema_file}
spaces:
- handler: Default
  roots:
  - path: ${fdb_path}
EOF

    cat > ${schema_file} << EOF
channel:Integer;
date:Date;
diagnostic:Integer;
direction:Integer;
expver:Expver;
fcmonth:Integer;
frequency:Integer;
grid:Grid;
hdate:Date;
ident:Integer;
instrument:Integer;
iteration:Integer;
latitude:Double;
levelist:Double;
longitude:Double;
method:Integer;
number:Integer;
param:Param;
refdate:Date;
step:Step;
system:Integer;
time:Time;
year:Integer;
month:Integer;
########################################################
# These are the rules for the Climate DT
 
# clte/wave
[ class?d1, dataset=climate-dt, activity, experiment, generation, model, realization, expver, stream=clte/wave, date
       [ resolution, type, levtype
               [ time, levelist?, param, frequency?, direction? ]]
]
 
# clmn
[ class?d1, dataset=climate-dt, activity, experiment, generation, model, realization, expver, stream=clmn, year
       [ month, resolution, type, levtype
               [ levelist?, param ]]
]
 
########################################################
# These are the rules for the Extremes DT
# oper/wave/lwda/lwwv
[ class?d1, dataset=extremes-dt, expver, stream=lwda/lwwv/oper/wave, date, time
        [ resolution, type, levtype
                [ step, levelist?, param, frequency?, direction? ]]
]
########################################################
# These are the rules for the On-Demand Extremes DT
# oper/wave
[ class?d1, dataset=on-demand-extremes-dt, expver, stream=oper/wave, date, time
        [ type, levtype
                [ step, levelist?, param, frequency?, direction? ]]
]
########################################################
# These are the rules for rd (to support legacy)
# oper/wave/lwda/lwwv
[ class?rd, expver, stream=dcda/lwda/lwwv/oper/wave, date, time, domain?
        [ type, levtype
                [ step, levelist?, param, frequency?, direction? ]]
]
 
[ class?rd, expver, stream=mnth, domain?
       [ type, levtype
               [ date , time, step?, levelist?, param ]]
]
########################################################
EOF
}