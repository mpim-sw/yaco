#!/bin/bash


generate_yaco_config_monthly_fine(){
  # some constants based on this experiment
  oce_levels=72
  soil_levels=5
  oce_halflevels=$(( oce_levels + 1 ))
  yaco_lag=1
  yacoTimeStep=${yacoTimeStep}
  atmos_time_step_in_sec=360
  ocean_time_step_in_sec=1800
  npressure_levels=19
  pressure_levels="100.0,500.0,1000.0,2000.0,3000.0,5000.0,7000.0,10000.0,15000.0,20000.0,25000.0,30000.0,40000.0,50000.0,60000.0,70000.0,85000.0,92500.0,100000.0"
  pressure_levels_fdb="1,5,10,20,30,50,70,100,150,200,250,300,400,500,600,700,850,925,1000"

  local nside=$1
  local rank=$2
  local filename=$3
  local resol=$4
  local fdb_config_file=$5

  echo "creating $filename"
  cat > $filename << EOF
definitions:
  fdb_pressure_levels: &fdb_pressure_levels # need to set those for FDB explicitely to ensure hPa unit
    levelist:
      source: yaco
      key: collection_index
      lookup: direct
      lookup_table: [ $pressure_levels_fdb ]

  fdb_levelist: &fdb_levelist
    levelist:
      source: grib
      key: mars.levelist
      maybeempty: true

  grib_ocean_halflevels: &grib_ocean_halflevels
    scaledValueOfFirstFixedSurface:
      source: yaco
      key: collection_index
      offset: 1
    scaleFactorOfFirstFixedSurface: 0

  grib_ocean_levels: &grib_ocean_levels
    scaledValueOfFirstFixedSurface:
      source: yaco
      key: collection_index
    scaleFactorOfFirstFixedSurface: 0
    scaledValueOfSecondFixedSurface:
      source: yaco
      key: collection_index
      offset: 1
    scaleFactorOfSecondFixedSurface: 0

  grib_soil_levels: &grib_soil_levels
    scaledValueOfFirstFixedSurface:
      source: yaco
      key: collection_index
    scaleFactorOfFirstFixedSurface: 0
    scaledValueOfSecondFixedSurface:
      source: yaco
      key: collection_index
      offset: 1
    scaleFactorOfSecondFixedSurface: 0

  grib_pressure_levels: &grib_pressure_levels
    scaledValueOfFirstFixedSurface:
      source: yaco
      key: collection_index
      lookup: direct
      lookup_table: [ ${pressure_levels} ]
    scaleFactorOfFirstFixedSurface: 0

  yac_input_default: &yac_input_default
    calendar: gregorian
    end_date: ${end_date}
    start_date: ${start_date}
    time_lag: ${yaco_lag}
    time_step: ${yacoTimeStep}
    yac_yaml: coupling.yaml

  grib_statistical_output: &grib_statistical_output
    productDefinitionTemplateNumber: 8 # statistics over time

  grib_statistical_range: &grib_statistical_range
    typeOfTimeIncrement: 255
    yearOfEndOfOverallTimeInterval:
      source: yaco
      key: timestep_end
      format: "%Y"
    monthOfEndOfOverallTimeInterval:
      source: yaco
      key: timestep_end
      format: "%m"
    dayOfEndOfOverallTimeInterval:
      source: yaco
      key: timestep_end
      format: "%d"
    hourOfEndOfOverallTimeInterval:
      source: yaco
      key: timestep_end
      format: "%H"
    minuteOfEndOfOverallTimeInterval:
      source: yaco
      key: timestep_end
      format: "%M"
    secondOfEndOfOverallTimeInterval:
      source: yaco
      key: timestep_end
      format: "%S"

  grib_time_span_1m: &grib_time_span_1m
    indicatorOfUnitForTimeRange: h
    lengthOfTimeRange: 744
    indicatorOfUnitForTimeIncrement: s
    timeIncrement: ${monthly_mean_time_step_in_sec}
    <<: *grib_statistical_range
#------- variable metadata ------------------
fdb_writer:
  collections:
    #
    # Atmosphere 2D variables (Monthly)
    #
    cllvi: # 235087 Time-mean total column liquid water
      grib_metadata:
        - discipline: 0
        - parameterCategory: 1
        - parameterNumber: 69
        - vertical.typeOfLevel: entireAtmosphere
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
      fdb_metadata:
        - *fdb_levelist
    clivi: # 235088 Time-mean total column cloud ice water
      grib_metadata:
        - discipline: 0
        - parameterCategory: 1
        - parameterNumber: 70
        - vertical.typeOfLevel: entireAtmosphere
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
      fdb_metadata:
        - *fdb_levelist
    pres_sfc: # 235134 Time-mean surface pressure
      grib_metadata:
        - discipline: 0
        - parameterCategory: 3
        - parameterNumber: 0
        - vertical.typeOfLevel: surface
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
    tcw: # 235136 Time-mean total column water
      grib_metadata:
        - discipline: 0
        - parameterCategory: 1
        - parameterNumber: 51
        - vertical.typeOfLevel: entireAtmosphere
        - typeOfFirstFixedSurface: 1
        - typeOfSecondFixedSurface: 8
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
    prw: # 235137 Time-mean total column vertically-integrated water vapour
      grib_metadata:
        - discipline: 0
        - parameterCategory: 1
        - parameterNumber: 64
        - vertical.typeOfLevel: entireAtmosphere
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
      fdb_metadata:
        - *fdb_levelist
    hydro_weq_snow_box: # 235078 Time-mean snow depth water equivalent
      grib_metadata:
        - discipline: 0
        - parameterCategory: 1
        - parameterNumber: 60
        - vertical.typeOfLevel: surface
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
    pres_msl: # 235151 Time-mean mean sea level pressure
      grib_metadata:
        - discipline: 0
        - parameterCategory: 3
        - parameterNumber: 0
        - vertical.typeOfLevel: meanSea
        - scaledValueOfFirstFixedSurface: 0
        - scaleFactorOfFirstFixedSurface: 0
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
    clt: # 235288 Time-mean total cloud cover
      grib_metadata:
        - discipline: 0
        - parameterCategory: 6
        - parameterNumber: 1
        - vertical.typeOfLevel: entireAtmosphere
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
      fdb_metadata:
        - *fdb_levelist
    uas: # 235165 Time-mean 10 metre U wind component
      grib_metadata:
        - discipline: 0
        - parameterCategory: 2
        - parameterNumber: 2
        - vertical.typeOfLevel: heightAboveGround
        - scaledValueOfFirstFixedSurface: 10
        - scaleFactorOfFirstFixedSurface: 0
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
    vas: # 235166 Time-mean 10 metre V wind component
      grib_metadata:
        - discipline: 0
        - parameterCategory: 2
        - parameterNumber: 3
        - vertical.typeOfLevel: heightAboveGround
        - scaledValueOfFirstFixedSurface: 10
        - scaleFactorOfFirstFixedSurface: 0
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
    tas: # 228004 Time-mean 2 metre temperature
      grib_metadata:
        - discipline: 0
        - parameterCategory: 0
        - parameterNumber: 0
        - vertical.typeOfLevel: heightAboveGround
        - scaledValueOfFirstFixedSurface: 2
        - scaleFactorOfFirstFixedSurface: 0
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
    dew2: # 235168 Time-mean 2 metre dewpoint temperature
      grib_metadata:
        - discipline: 0
        - parameterCategory: 0
        - parameterNumber: 6
        - vertical.typeOfLevel: heightAboveGround
        - scaledValueOfFirstFixedSurface: 2
        - scaleFactorOfFirstFixedSurface: 0
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
    sfcwind: # 228005 Time-mean 10 metre wind speed
      grib_metadata:
        - discipline: 0
        - parameterCategory: 2
        - parameterNumber: 1
        - typeOfFirstFixedSurface: 103
        - scaledValueOfFirstFixedSurface: 10
        - scaleFactorOfFirstFixedSurface: 0
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
    ts_rad: # 235079 Time-mean skin temperature
      grib_metadata:
        - discipline: 0
        - parameterCategory: 0
        - parameterNumber: 17
        - vertical.typeOfLevel: surface
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
    hydro_runoff_box: # 235020 Time-mean surface runoff rate
      grib_metadata:
        - discipline: 2
        - parameterCategory: 0
        - parameterNumber: 51
        - vertical.typeOfLevel: surface
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
      fdb_metadata:
        - *fdb_levelist
    hydro_drainage_box: # 235021 Time-mean sub-surface runoff rate
      grib_metadata:
        - discipline: 2
        - parameterCategory: 0
        - parameterNumber: 52
        - vertical.typeOfLevel: surface
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
      fdb_metadata:
        - *fdb_levelist
    prls: # 235031 Time-mean total snowfall rate water equivalent
      grib_metadata:
        - discipline: 0
        - parameterCategory: 1
        - parameterNumber: 53
        - vertical.typeOfLevel: surface
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
      fdb_metadata:
        - *fdb_levelist
    hfss: # 235033 Time-mean surface sensible heat flux
      grib_metadata:
        - discipline: 0
        - parameterCategory: 0
        - parameterNumber: 11
        - vertical.typeOfLevel: surface
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
    hfls: # 235034 Time-mean surface latent heat flux
      grib_metadata:
        - discipline: 0
        - parameterCategory: 0
        - parameterNumber: 10
        - vertical.typeOfLevel: surface
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
    rsdt: # 235053 Mean top downward short-wave radiation flux
      grib_metadata:
        - discipline: 0
        - parameterCategory: 4
        - parameterNumber: 7
        - vertical.typeOfLevel: nominalTop
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
      fdb_metadata:
        - *fdb_levelist
    rsds: # 235035 Mean surface downward short-wave radiation flux
      grib_metadata:
        - discipline: 0
        - parameterCategory: 4
        - parameterNumber: 7
        - vertical.typeOfLevel: surface
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
    rlds: # 235036 mean surface downward long-wave radiation flux
      grib_metadata:
        - discipline: 0
        - parameterCategory: 5
        - parameterNumber: 3
        - vertical.typeOfLevel: surface
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
    rsns: # 235037 Mean surface net short-wave radiation flux
      grib_metadata:
        - discipline: 0
        - parameterCategory: 4
        - parameterNumber: 9
        - vertical.typeOfLevel: surface
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
    rlns: # 235038 Mean surface net long-wave radiation flux
      grib_metadata:
        - discipline: 0
        - parameterCategory: 5
        - parameterNumber: 5
        - vertical.typeOfLevel: surface
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
    rsnt: # 235039 Mean top net short-wave radiation flux
      grib_metadata:
        - discipline: 0
        - parameterCategory: 4
        - parameterNumber: 9
        - vertical.typeOfLevel: nominalTop
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
      fdb_metadata:
        - *fdb_levelist
    rlnt: # 235040 Mean top net long-wave radiation flux
      grib_metadata:
        - discipline: 0
        - parameterCategory: 5
        - parameterNumber: 5
        - vertical.typeOfLevel: nominalTop
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
      fdb_metadata:
        - *fdb_levelist
    rsnscs: # 235051 Mean surface net short-wave radiation flux, clear sky
      grib_metadata:
        - discipline: 0
        - parameterCategory: 4
        - parameterNumber: 11
        - vertical.typeOfLevel: surface
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
    rlnscs: # 235052 Mean surface net long-wave radiation flux, clear sky
      grib_metadata:
        - discipline: 0
        - parameterCategory: 5
        - parameterNumber: 6
        - vertical.typeOfLevel: surface
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
    rsntcs: # 235049 Mean top net short-wave radiation flux, clear sky
      grib_metadata:
        - discipline: 0
        - parameterCategory: 4
        - parameterNumber: 11
        - vertical.typeOfLevel: nominalTop
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
      fdb_metadata:
        - *fdb_levelist
    rlntcs: # 235050 Mean top net long-wave radiation flux, clear sky
      grib_metadata:
        - discipline: 0
        - parameterCategory: 5
        - parameterNumber: 6
        - vertical.typeOfLevel: nominalTop
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
      fdb_metadata:
        - *fdb_levelist
    tauu: # 235041 Time-mean eastward turbulent surface stress
      grib_metadata:
        - discipline: 0
        - parameterCategory: 2
        - parameterNumber: 62
        - vertical.typeOfLevel: surface
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
    tauv: # 235042 Time-mean northward turbulent surface stress
      grib_metadata:
        - discipline: 0
        - parameterCategory: 2
        - parameterNumber: 63
        - vertical.typeOfLevel: surface
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
    evspsbl: # 235043 Mean evaporation rate
      grib_metadata:
        - discipline: 0
        - parameterCategory: 1
        - parameterNumber: 79
        - vertical.typeOfLevel: surface
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
      fdb_metadata:
        - *fdb_levelist
    pr: # 235055 Mean Total precipitation rate
      grib_metadata:
        - discipline: 0
        - parameterCategory: 1
        - parameterNumber: 52
        - vertical.typeOfLevel: surface
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
      fdb_metadata:
        - *fdb_levelist
    #
    # Soil (Monthly)
    #
    hydro_vol_weq_soil_sl_box: # 235077 Time-mean volumetric soil moisture
      grib_metadata:
        - discipline: 2
        - parameterCategory: 0
        - parameterNumber: 25
        - vertical.typeOfLevel: soilLayer
        - *grib_soil_levels
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
      fdb_metadata:
        - *fdb_levelist
    #
    # Sea Ice (Monthly)
    #
    hi: # 263000 Time-mean sea ice thickness
      grib_metadata:
        - discipline: 10
        - parameterCategory: 2
        - parameterNumber: 1
        - vertical.typeOfLevel: iceLayerOnWater
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
      fdb_metadata:
        - *fdb_levelist
    conc: # 263001 Time-mean sea ice area fraction
      grib_metadata:
        - discipline: 10
        - parameterCategory: 2
        - parameterNumber: 0
        - vertical.typeOfLevel: iceTopOnWater
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
      fdb_metadata:
        - *fdb_levelist
    ice_u: # 263003 Time-mean eastward sea ice velocity
      grib_metadata:
        - discipline: 10
        - parameterCategory: 2
        - parameterNumber: 4
        - vertical.typeOfLevel: iceLayerOnWater
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
      fdb_metadata:
        - *fdb_levelist
    ice_v: # 263004 Time-mean northward sea ice velocity
      grib_metadata:
        - discipline: 10
        - parameterCategory: 2
        - parameterNumber: 5
        - vertical.typeOfLevel: iceLayerOnWater
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
      fdb_metadata:
        - *fdb_levelist
    sivol: # 263008 Time-mean sea ice volume per unit area
      grib_metadata:
        - discipline: 10
        - parameterCategory: 2
        - parameterNumber: 15
        - vertical.typeOfLevel: iceLayerOnWater
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
      fdb_metadata:
        - *fdb_levelist
    snvol: # 263009 Time-mean snow volume over sea ice per unit area
      grib_metadata:
        - discipline: 10
        - parameterCategory: 2
        - parameterNumber: 16
        - vertical.typeOfLevel: snowLayerOverIceOnWater
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
      fdb_metadata:
        - *fdb_levelist
    #
    # Ocean 2D (Monthly)
    #
    tos: # 263101 Time-mean sea surface temperature
      grib_metadata:
        - discipline: 10
        - parameterCategory: 3
        - parameterNumber: 0
        - vertical.typeOfLevel: oceanSurface
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
      fdb_metadata:
        - *fdb_levelist
    sos: # 263100 Time-mean sea surface practical salinity
      grib_metadata:
        - discipline: 10
        - parameterCategory: 3
        - parameterNumber: 3
        - vertical.typeOfLevel: oceanSurface
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
      fdb_metadata:
        - *fdb_levelist
    heat_content_300m: # 263121 Time-mean vertically-integrated heat content in the upper 300m
      grib_metadata:
        - discipline: 10
        - parameterCategory: 4
        - parameterNumber: 22
        - vertical.typeOfLevel: depthBelowSeaLayer
        - scaleFactorOfFirstFixedSurface: 0
        - scaledValueOfFirstFixedSurface: 0
        - scaleFactorOfSecondFixedSurface: 0
        - scaledValueOfSecondFixedSurface: 300
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
      fdb_metadata:
        - *fdb_levelist
    heat_content_700m:  # 263122 Time-mean vertically-integrated heat content in the upper 700m
      grib_metadata:
        - discipline: 10
        - parameterCategory: 4
        - parameterNumber: 22
        - vertical.typeOfLevel: depthBelowSeaLayer
        - scaleFactorOfFirstFixedSurface: 0
        - scaledValueOfFirstFixedSurface: 0
        - scaleFactorOfSecondFixedSurface: 0
        - scaledValueOfSecondFixedSurface: 700
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
      fdb_metadata:
        - *fdb_levelist
    heat_content_total: # 263123 Time-mean total column heat content
      grib_metadata:
        - discipline: 10
        - parameterCategory: 4
        - parameterNumber: 22
        - vertical.typeOfLevel: oceanSurfaceToBottom
        - scaleFactorOfFirstFixedSurface: 0
        - scaledValueOfFirstFixedSurface: 0
        - scaleFactorOfSecondFixedSurface: MISSING
        - scaledValueOfSecondFixedSurface: MISSING
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
      fdb_metadata:
        - *fdb_levelist
    ssh: # 263124 Time-mean sea surface height
      grib_metadata:
        - discipline: 10
        - parameterCategory: 3
        - parameterNumber: 1
        - vertical.typeOfLevel: oceanSurface
        - scaledValueOfFirstFixedSurface: 0
        - scaleFactorOfFirstFixedSurface: 0
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
      fdb_metadata:
        - *fdb_levelist
    #
    # Ocean 3D variables (Monthly)
    #
    so: # 263500 Time-mean sea water practical salinity
      grib_metadata:
        - discipline: 10
        - parameterCategory: 4
        - parameterNumber: 21
        - vertical.typeOfLevel: oceanModelLayer
        - *grib_ocean_levels
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
      fdb_metadata:
        - *fdb_levelist
    to: # 263501 Time-mean sea water potential temperature
      grib_metadata:
        - discipline: 10
        - parameterCategory: 4
        - parameterNumber: 18
        - vertical.typeOfLevel: oceanModelLayer
        - *grib_ocean_levels
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
      fdb_metadata:
        - *fdb_levelist
    v: # 263505 Time-mean northward sea water velocity
      grib_metadata:
        - discipline: 10
        - parameterCategory: 4
        - parameterNumber: 24
        - vertical.typeOfLevel: oceanModelLayer
        - *grib_ocean_levels
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
      fdb_metadata:
        - *fdb_levelist
    u: # 263506 Time-mean eastward sea water velocity
      grib_metadata:
        - discipline: 10
        - parameterCategory: 4
        - parameterNumber: 23
        - vertical.typeOfLevel: oceanModelLayer
        - *grib_ocean_levels
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
      fdb_metadata:
        - *fdb_levelist
    w: # 263507 Time-mean upward sea water velocity
      grib_metadata:
        - discipline: 10
        - parameterCategory: 4
        - parameterNumber: 27
        - vertical.typeOfLevel: oceanModel
        - *grib_ocean_halflevels
        - *grib_statistical_output
        - typeOfStatisticalProcessing: avg
        - *grib_time_span_1m
      fdb_metadata:
        - *fdb_levelist
  fdb_config: ${fdb_config_file}
  fdb_metadata:
    - class:
        source: grib
        key: class
    - dataset:
        source: grib
        key: dataset
    - activity:
        source: grib
        key: activity
    - experiment:
        source: grib
        key: experiment
    - generation:
        source: grib
        key: generation
    - model:
        source: grib
        key: model
    - realization:
        source: grib
        key: realization
    - expver:
        source: grib
        key: experimentVersionNumber
    - stream:
        source: grib
        key: marsStream
    - resolution:
        source: grib
        key: resolution
    - type:
        source: grib
        key: mars.type
    - param:
        source: grib
        key: paramId
    - levtype:
        source: grib
        key: mars.levtype
    - year:
        source: grib
        key: year
    - month:
        source: grib
        key: month
  grib_metadata:
    - centre: ecmf
    - subCentre: 1003
    - tablesVersion: 34
    - typeOfProcessedData: fc
    - setLocalDefinition: 1 # write section 2
    - significanceOfReferenceTime: 2
    - year:
        source: yaco
        key: timestep_begin
        format: "%Y"
    - month:
        source: yaco
        key: timestep_begin
        format: "%m"
    - day:
        source: yaco
        key: timestep_begin
        format: "%d"
    - hour:
        source: yaco
        key: timestep_begin
        format: "%H"
    - minute:
        source: yaco
        key: timestep_begin
        format: "%M"
    - second:
        source: yaco
        key: timestep_begin
        format: "%S"
    - productionStatusOfProcessedData: 12 # 12=destine production, 13=destine test
    - resolutionAndComponentFlags: 0
    - generatingProcessIdentifier: 255
    - hoursAfterDataCutoff: 65535
    - minutesAfterDataCutoff: 255
    - dataset: climate-dt
    - class: d1
    - activity: projections
    - experiment: SSP3-7.0
    - generation: 2
    - mars.stream: clmn
    - mars.type: fc
    - model: ICON
    - backgroundProcess: 2
    - realization: 1
    - resolution: ${resol}
    - experimentVersionNumber: 0001
    - gridType:
        source: grid
        key: type
    - Nside:
        source: grid
        key: sides
    - orderingConvention:
        source: grid
        key: order
    - shapeOfTheEarth: 6
    - longitudeOfFirstGridPoint: 45000000
    - bitsPerValue: 16 # has to be set BEFORE packingType
    - packingType: grid_ccsds
    - missingValue:
        source: yaco
        key: missing_value
    - bitmapPresent:
        source: yaco
        key: has_missing_values
    - values:
        source: data
grid:
  name: healpix_${nside} # as identified in YAC
  nside: ${nside}
  order: nested
  type: healpix
output: fdb
yac_input:
  ranks:
    $(( rank + 0 )):
      <<: *yac_input_default
      name: yaco_${nside}_rank_$(( rank + 0 ))_monthly
      collections:
        - name: pr
          size: 1
        - name: tas
          size: 1
        - name: clt
          size: 1
        - name: uas
          size: 1
        - name: vas
          size: 1
        - name: dew2
          size: 1
        - name: hfss
          size: 1
        - name: hfls
          size: 1
        - name: clivi
          size: 1
        - name: cllvi
          size: 1
        - name: pres_sfc
          size: 1
        - name: pres_msl
          size: 1
        - name: ts_rad
          size: 1
        - name: prw
          size: 1
        - name: tauu
          size: 1
        - name: tauv
          size: 1
        - name: tcw
          size: 1
        - name: sfcwind
          size: 1
    $(( rank + 1 )):
      <<: *yac_input_default
      name: yaco_${nside}_rank_$(( rank + 1 ))_monthly
      collections:
        - name: rsdt
          size: 1
        - name: rsds
          size: 1
        - name: rlds
          size: 1
        - name: rsns
          size: 1
        - name: rsnt
          size: 1
        - name: rlns
          size: 1
        - name: rlnt
          size: 1
        - name: rsnscs
          size: 1
        - name: rsntcs
          size: 1
        - name: rlnscs
          size: 1
        - name: rlntcs
          size: 1

    $(( rank + 2 )):
      <<: *yac_input_default
      name: yaco_${nside}_rank_$(( rank + 2 ))_monthly
      collections:
        - name: conc
          size: 1
        - name: hi
          size: 1
        - name: ice_u
          size: 1
        - name: ice_v
          size: 1
        - name: ssh
          size: 1
        - name: sivol
          size: 1
        - name: snvol
          size: 1
        - name: tos
          size: 1
        - name: sos
          size: 1
    $(( rank + 3 )):
      <<: *yac_input_default
      name: yaco_${nside}_rank_$(( rank + 3 ))_monthly
      collections:
        - name: to
          size: ${oce_levels}
        - name: so
          size: ${oce_levels}
        - name: heat_content_300m
          size: 1
        - name: heat_content_700m
          size: 1
        - name: heat_content_total
          size: 1
        - name: u
          size: ${oce_levels}
        - name: v
          size: ${oce_levels}
        - name: w
          size: ${oce_halflevels}
    $(( rank + 4 )):
      <<: *yac_input_default
      name: yaco_${nside}_rank_$(( rank + 4 ))_monthly
      collections:
        - name: prls
          size: 1
        - name: evspsbl
          size: 1
        - name: hydro_runoff_box
          size: 1
        - name: hydro_drainage_box
          size: 1
        - name: hydro_weq_snow_box
          size: 1
        - name: hydro_vol_weq_soil_sl_box
          size: ${soil_levels}
EOF
}