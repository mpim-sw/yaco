#!/bin/bash
set -e

# PLEASE READ THE COMMENTS BEFORE TOUCHING ANYTHING IN THIS FILE
# The generated gsv works only with the coupling configuration provided in this folder
# Note the additional variables which are needed to be configured depending on the mpi processes used

# ---- 
# the generator function is incrementally hacked to fit the ever-changing requirements
# this is by no means the recommended way of doing things 
# the weird arguments (specially in low-res monthly config) seen here keep the number of lines to a "minimum" in the given timeframe
# things are called (eg., high, standard, fine, coarse, clte, clmn, etc) the way they are called due to the incremental requirements of the project
# efforts to streamline them will be made in the next phase

# generate yaco config for (in the order)
# non-monthly variables, high resolution
# non-monthly variables, low resolution 
# monthly variables, high resolution (no pressure levels)
# monthly variables, low resolution (full set monthly)

# generate the yaco gsv files
yaco_min_rank_fine_non_monthly=$1
yaco_min_rank_coarse_non_monthly=$2

yaco_min_rank_fine_monthly=$3
yaco_min_rank_coarse_monthly=$4

nside_fine=$5
nside_coarse=$6

export start_date=$7
export end_date=$8
fdb_path=$9

export activity=${10}
export experiment=${11}
export generation=${12}
export realization=${13}
export expver=${14}
# add the generator scripts here
this_dir=$(dirname "$0")
. ${this_dir}/generate_yaco_config_fine_monthly.sh 
. ${this_dir}/generate_yaco_config_coarse_monthly.sh
. ${this_dir}/generate_yaco_config_coarse_non_monthly.sh
. ${this_dir}/generate_yaco_config_fine_non_monthly.sh

# some constants based on this experiment
export oce_levels=72
export soil_levels=5
export oce_halflevels=$(( oce_levels + 1 ))
export yaco_lag=1
export yacoTimeStep="PT1H"
export atmos_time_step_in_sec=360
export ocean_time_step_in_sec=1800
export monthly_mean_time_step_in_sec=3600
export npressure_levels=19
export pressure_levels="100.0,500.0,1000.0,2000.0,3000.0,5000.0,7000.0,10000.0,15000.0,20000.0,25000.0,30000.0,40000.0,50000.0,60000.0,70000.0,85000.0,92500.0,100000.0"
export pressure_levels_fdb="1,5,10,20,30,50,70,100,150,200,250,300,400,500,600,700,850,925,1000"



# call with appropriate arguments to create the yamls
# 1 yaco task per nside-level (TODO: the yac_input_ranks for yaco needs to be derived and extended appropriately)
# some constants used for yaco relevant parts

generate_yaco_config_fine_non_monthly $nside_fine $yaco_min_rank_fine_non_monthly ./gsv_fine_hourly_daily.yaml "high" ${fdb_path}
generate_yaco_config_coarse_non_monthly $nside_coarse $yaco_min_rank_coarse_non_monthly ./gsv_coarse_hourly_daily.yaml "standard" ${fdb_path}

generate_yaco_config_monthly_fine $nside_fine $yaco_min_rank_fine_monthly ./gsv_fine_monthly.yaml "high" ${fdb_path}
generate_yaco_config_monthly_coarse $nside_coarse $yaco_min_rank_coarse_monthly ./gsv_coarse_monthly.yaml "standard" ${fdb_path}

