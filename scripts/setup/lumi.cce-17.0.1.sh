#!/bin/bash
set -o errexit -o pipefail -o noclobber -o nounset

DEPENDENCY_TO_BUILD="all"
while [ $# -gt 0 ]; do
    case "$1" in
        --yaco-root)
            YACO_ROOT=$2
            ABSOLUTE_YACO_ROOT=$(readlink -f "${YACO_ROOT}")
            shift 2
            ;;
        --dependency)
            DEPENDENCY_TO_BUILD=$2
            shift 2
            ;;
        *)
            echo "Error: unknown argument '$1'. Call this script with --yaco-root and path to your yaco-root"
            exit 1
            ;;
    esac
done

echo "Absolute path provided as the yaco root: $ABSOLUTE_YACO_ROOT"

# take the right compiler
COMPILER="cce-17.0.1"
module purge
module load PrgEnv-cray cce/17.0.1 craype-x86-milan cray-mpich/8.1.29 cray-hdf5 cray-netcdf cray-libsci libfabric
unset INCLUDE_PATH_X86_64

export CC="cc"
export CXX="CC"
export FC="ftn"

THREADS=64

# get current yaco commit hash
pushd $ABSOLUTE_YACO_ROOT 
YACO_HASH=$(git log --pretty=format:'%h' -n 1)
popd

BUILD_PATH=$(pwd)/$YACO_HASH-$COMPILER
INSTALL_PATH=$BUILD_PATH/install
mkdir -p "$BUILD_PATH"
pushd "$BUILD_PATH"

# reason to keep seperate build scripts is to allow for any possible special 
# build flags later which may be machine specific

# build dependencies for yaco
echo "=== Building yaco dependencies ==="
echo "Dependencies to build: $DEPENDENCY_TO_BUILD"
CC="${CC}" CXX="${CXX}" FC="${FC}" $ABSOLUTE_YACO_ROOT/scripts/setup/build-dependencies.sh -j $THREADS --dependency $DEPENDENCY_TO_BUILD --prefix_path $INSTALL_PATH

# build yaco
echo "=== Building yaco ==="
CC="${CC}" CXX="${CXX}" FC="${FC}" cmake $ABSOLUTE_YACO_ROOT -DCMAKE_PREFIX_PATH=$INSTALL_PATH -DENABLE_NETCDF=OFF -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DCMAKE_BUILD_TYPE=Release
cmake --build . -j $THREADS
popd
