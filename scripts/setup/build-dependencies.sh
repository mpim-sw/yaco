#!/bin/bash
# More safety by turning some bugs into errors.
# Without `errexit` you don’t need ! and can replace
# ${PIPESTATUS[0]} with a simple $?, but I prefer safety.
set -o errexit -o pipefail -o noclobber


REPO_TAG_AEC="v1.1.2"
REPO_TAG_ECBUILD="3.8.0"
REPO_TAG_ECCODES="2.39.0"
REPO_TAG_ECKIT="1.27.0"
REPO_TAG_METKIT="1.11.9"
REPO_TAG_FDB="5.12.0"
REPO_TAG_FYAML="v0.9"
REPO_TAG_CFITSIO="4.1.0"
REPO_TAG_HEALPIX="trunk" # because its only a mirror. version control is done else where.
REPO_TAG_MTIME="release-1.2.2" # using a non-release tag will require autoreconf later
REPO_TAG_YAXT="release-0.11.3" 
REPO_TAG_YAC="release-3.4.0_p2" # using a non-release tag will require autoreconf later


function usage {
    echo "This script is intendet to build the dependencies of YACO"
    echo "Each checked out branch/version of the repositories will be built under build_dir_name/external/repo/build folder"
    echo "All libraries built will be installed under --prefix-path provided"
    echo "The script can also be used to build only one or the other dependency in one call"
    echo ""
    echo "--- Workflow --------------------------------------------------------------"
    echo "   build-dependencies -j <thread_count> --build_dir_name <my-compiler-v9999> --prefix_path <install> "
    echo "--- Options ---------------------------------------------------------------"
    echo "   -j                              Threadcount"
    echo "   --build_dir_name <name>         Sets the name of the work dir folder which"
    echo "                                   will be created in the process where libraries will be built like $(pwd)/<name>."
    echo "   --prefix_path <install_path>    Path where built libraries will be installed"
    echo "   --dependency <name>/all         Set the library name to be built eg: healpix"
    echo "                                   valid is one of [aec, fdb, ecbuild, eccodes, eckit, metkit, fyaml, cfitsio, healpix, yac, yaxt, mtime]" 
    echo "                                   defaults to building all depedencies"
}


function clone_to {
    if [ -d "$2" ]
    then
        pushd "$2"
        echo "repo already loaded: skipping git clone"
        popd
    else
        if [ -z "$3" ]; then
            echo "Using default branch"
            git clone --depth=1 --recursive "$1" "$2"
        else
            git clone --depth=1 --recursive --branch "$3" "$1" "$2"
        fi
    fi
}

# invoke with the specified FC, CC and CXX if they are present as variables
function invoke_build_script_generation {
    if [ -n "${CC}" ] || [ -n "${CXX}" ] || [ -n "${FC}" ]; then
    echo "invoking.. env CC=${CC:-$CC} CXX=${CXX:-$CXX} FC=${FC:-$FC} "$@""
	env CC=${CC:-$CC} CXX=${CXX:-$CXX} FC=${FC:-$FC} "$@"
    else
        "$@"
    fi
}


function install_via_cmake {
    REPO_NAME=$1
    shift
    REPO_TAG=$1
    shift
    GIT_URL=$1
    shift

    local REPO_TAG=${REPO_TAG/\//-}
    local VERSIONED_FOLDER=$REPO_NAME-$REPO_TAG
    local REPO_PATH=$GIT_REPOS/$VERSIONED_FOLDER
    local BUILD_FOLDER=$REPO_PATH/build

    clone_to "$GIT_URL" "$REPO_PATH" "$REPO_TAG"
    mkdir -p "$BUILD_FOLDER"
    pushd "$BUILD_FOLDER"

    invoke_build_script_generation cmake "$REPO_PATH" -DCMAKE_INSTALL_PREFIX="$INSTALL_DIR" -DCMAKE_PREFIX_PATH="$INSTALL_DIR" "$@"
    make -j "$THREADS"
    make install
    popd
}

function install_aec {
    FOLDER_NAME="aec"
    GIT_URL=git@gitlab.dkrz.de:k202009/libaec.git

    install_via_cmake "$FOLDER_NAME" "$REPO_TAG_AEC" "$GIT_URL"

}

function install_ecbuild {
    FOLDER_NAME="ecbuild"
    GIT_URL=https://github.com/ecmwf/ecbuild

    install_via_cmake "$FOLDER_NAME" "$REPO_TAG_ECBUILD" "$GIT_URL"

}

function install_eccodes {
    REPO_NAME="eccodes"
    GIT_URL=https://github.com/ecmwf/eccodes

    install_via_cmake "$REPO_NAME" "$REPO_TAG_ECCODES" "$GIT_URL" -DENABLE_AEC=ON
}

function install_eckit {
    REPO_NAME="eckit"
    GIT_URL="https://github.com/ecmwf/eckit"

    install_via_cmake "$REPO_NAME" "$REPO_TAG_ECKIT" "$GIT_URL" 
}

function install_metkit {
    REPO_NAME="metkit"
    GIT_URL="https://github.com/ecmwf/metkit"

    install_via_cmake "$REPO_NAME" "$REPO_TAG_METKIT" "$GIT_URL" 
}

function install_fdb {
    REPO_NAME="fdb"
    GIT_URL="https://github.com/ecmwf/fdb"

    install_via_cmake "$REPO_NAME" "$REPO_TAG_FDB" "$GIT_URL" 
}

function install_fyaml {
    GIT_URL="https://github.com/pantoniou/libfyaml.git"
    REPO_NAME="fyaml"

    install_via_cmake "$REPO_NAME" "$REPO_TAG_FYAML" "$GIT_URL"
}

function install_cfitsio {
    REPO_NAME="cfitsio"
    GIT_URL="https://github.com/healpy/cfitsio.git"

    install_via_cmake "$REPO_NAME" "$REPO_TAG_CFITSIO" "$GIT_URL"
}

function install_healpix {
    REPO_NAME="healpix"

    VERSIONED_FOLDER=$REPO_NAME-$REPO_TAG_HEALPIX
    REPO_PATH=$GIT_REPOS/$VERSIONED_FOLDER
    #-------------------------------------------------------------------------------------
    clone_to https://github.com/healpy/healpixmirror.git "$REPO_PATH" "$REPO_TAG_HEALPIX"
    #-------------------------------------------------------------------------------------
    # install libsharp
    LIBSHARP_ROOT=$REPO_PATH/src/common_libraries/libsharp
    LIBSHARP_BUILD_FOLDER=$LIBSHARP_ROOT/build

    pushd "$LIBSHARP_ROOT"
    autoreconf -ifv
    popd

    mkdir -p "$LIBSHARP_BUILD_FOLDER"
    pushd "$LIBSHARP_BUILD_FOLDER"
    invoke_build_script_generation "$LIBSHARP_ROOT"/configure --prefix="$INSTALL_DIR"

    make -j "$THREADS"
    make install
    popd

    #-------------------------------------------------------------------------------------
    HEALPIX_ROOT=$REPO_PATH/src/cxx
    HEALPIX_BUILD_DIR=$HEALPIX_ROOT/build

    ld_flags="-L$INSTALL_DIR/lib"
    if [ -d "$INSTALL_DIR/lib64" ]; then
        echo "appending lib64 folder to ldflags"
        ld_flags="$ld_flags -L$INSTALL_DIR/lib64 -Wl,-rpath,$INSTALL_DIR/lib64"
    fi

    pushd "$HEALPIX_ROOT"
    autoreconf -ifv
    popd

    mkdir -p "$HEALPIX_BUILD_DIR"
    pushd "$HEALPIX_BUILD_DIR"
    INCLUDE_FLAGS="-I$INSTALL_DIR/include"
    invoke_build_script_generation $HEALPIX_ROOT/configure --prefix=$INSTALL_DIR CFLAGS="$INCLUDE_FLAGS" CXXFLAGS="$INCLUDE_FLAGS" LDFLAGS="$ld_flags"

    make -j "$THREADS"
    make install
    popd
}

function install_mtime {
    REPO_NAME="mtime"
    VERSIONED_FOLDER=$REPO_NAME-$REPO_TAG_MTIME
    REPO_PATH=$GIT_REPOS/$VERSIONED_FOLDER
    #-------------------------------------------------------------------------------------
    clone_to https://gitlab.dkrz.de/icon-libraries/libmtime.git "$REPO_PATH" "$REPO_TAG_MTIME"
    #-------------------------------------------------------------------------------------
    
    MTIME_BUILD_FOLDER=$REPO_PATH/build
    mkdir -p "$MTIME_BUILD_FOLDER"
    pushd "$MTIME_BUILD_FOLDER"

    invoke_build_script_generation $REPO_PATH/configure --disable-maintainer-mode --enable-shared --disable-static --disable-examples --disable-fortran-hl --disable-python --prefix=$INSTALL_DIR

    make -j "$THREADS"
    make install
    popd
}

function install_yaxt {
    REPO_NAME="yaxt"
    VERSIONED_FOLDER=$REPO_NAME-$REPO_TAG_YAXT
    REPO_PATH=$GIT_REPOS/$VERSIONED_FOLDER
    #-------------------------------------------------------------------------------------
    clone_to https://gitlab.dkrz.de/dkrz-sw/yaxt.git "$REPO_PATH" "$REPO_TAG_YAXT"
    #-------------------------------------------------------------------------------------
    
    pushd "$REPO_PATH"
    autoreconf -ifv
    popd

    YAXT_BUILD_FOLDER=$REPO_PATH/build
    mkdir -p "$YAXT_BUILD_FOLDER"
    pushd "$YAXT_BUILD_FOLDER"

    invoke_build_script_generation $REPO_PATH/configure --disable-rpaths --disable-mpi-checks --enable-openmp --enable-grib2 --enable-loop-exchange --enable-fcgroup-O0 --enable-fcgroup-DIAG --disable-option-checking --enable-silent-rules=yes --disable-maintainer-mode --disable-shared --enable-static --with-pic=no --with-regard-for-quality=no --with-on-demand-check-programs --without-example-programs --prefix=$INSTALL_DIR

    make -j "$THREADS"
    make install
    popd
}

function install_yac {
    REPO_NAME="yac"
    VERSIONED_FOLDER=$REPO_NAME-$REPO_TAG_YAC
    REPO_PATH=$GIT_REPOS/$VERSIONED_FOLDER
    #-------------------------------------------------------------------------------------
    clone_to https://gitlab.dkrz.de/dkrz-sw/yac.git "$REPO_PATH" "$REPO_TAG_YAC"
    #-------------------------------------------------------------------------------------
    

    YAC_BUILD_FOLDER=$REPO_PATH/build
    mkdir -p "$YAC_BUILD_FOLDER"
    pushd "$YAC_BUILD_FOLDER"

    fyaml_lib_folders="$INSTALL_DIR/lib"
    if [ -d "$INSTALL_DIR/lib64" ]; then
        echo "appending lib64 folder"
        fyaml_lib_folders="$INSTALL_DIR/lib -L$INSTALL_DIR/lib64"
    fi

    invoke_build_script_generation $REPO_PATH/configure --disable-maintainer-mode --enable-loop-exchange --enable-fcgroup-O0 --enable-fcgroup-DIAG --disable-option-checking --disable-mpi-checks --disable-netcdf --prefix=$INSTALL_DIR \
        --with-yaxt-include=$INSTALL_DIR/include \
        --with-yaxt-lib=$INSTALL_DIR/lib \
        --with-fyaml-include=$INSTALL_DIR/include \
        --with-fyaml-lib="$fyaml_lib_folders"


    make -j "$THREADS"
    make install
    popd
}

function install_all {
    echo "========================"
    echo "---- handling aec"
    install_aec
    echo "------------------------"
    echo "---- handling ecbuild"
    install_ecbuild
    echo "------------------------"
    echo "---- handling eccodes"
    install_eccodes
    echo "------------------------"
    echo "---- handling eckit"
    install_eckit
    echo "------------------------"
    echo "---- handling metkit"
    install_metkit
    echo "------------------------"
    echo "---- handling fdb"
    install_fdb
    echo "========================"
    echo "== handling yaml"
    install_fyaml
    echo "========================"
    echo "== handling CFITSIO"
    install_cfitsio
    echo "========================"
    echo "== handling SHARP and HEALPIX"
    install_healpix
    echo "========================"
    echo "== handling MTIME, YAXT & YAC"
    install_mtime
    install_yaxt
    install_yac
    echo "========================"
}


BUILD_DIR_NAME="build_dependencies_$(date -I)"
THREADS=8

DEP_TO_BUILD_INSTALL="all" # can be overriden with any particular library to be built

while [[ $# -gt 0 ]]; do
    case "$1" in
        -j)
            THREADS=$2
            shift 2
            ;;
        --dependency)
            DEP_TO_BUILD_INSTALL=$2
            echo "Building the following dependency: $DEP_TO_BUILD_INSTALL"
            shift 2
            ;;
        --prefix_path)
            INSTALL_DIR=$2
            echo "Install prefix path set to: $INSTALL_DIR"
            shift 2
            ;;
        -h|--help)
            usage
            exit 0
            ;;
        *)
            echo "Unknown option: $1"
            usage
            exit 1
            ;;
    esac
done


GIT_REPOS=$(pwd)/external
mkdir -p "$GIT_REPOS"
echo "GIT_REPOS: $GIT_REPOS"

mkdir -p "$INSTALL_DIR"
echo "INSTALL_DIR: $INSTALL_DIR"

install_${DEP_TO_BUILD_INSTALL}
