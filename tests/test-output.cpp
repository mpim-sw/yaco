// SPDX-FileCopyrightText: 2023 Max Planck Institute for Meteorology, Yaco authors
//
// SPDX-License-Identifier: BSD-3-Clause

#include <iostream>
#include <yaco/chrono.hpp>
#include <yaco/collection.hpp>
#include <yaco/grid.hpp>
#include <yaco/yaco.hpp>

int main(int argc, char** argv)
{
    if (argc != 2) {
        std::cerr << "Usage: " << argv[0] << " <config_file>" << std::endl;
        return 1;
    }
    std::string config_filename = argv[1];

    yaco::config::Value config = yaco::config::Document::fromYaml(config_filename).value();

    std::unique_ptr<yaco::DataHandler> data_handler;
    yaco::config::Value collections_config;
    {
        const auto output = config.at("output").asString();
#ifdef YACO_ENABLE_NETCDF
        if (output == "netcdf") {
            data_handler       = yaco::DataHandler::createNetCDFWriter(config.at("netcdf_writer"));
            collections_config = config.at("netcdf_writer").at("collections");
        } else
#endif
#ifdef YACO_ENABLE_FDB
                if (output == "fdb") {
            data_handler       = yaco::DataHandler::createFDBWriter(config.at("fdb_writer"));
            collections_config = config.at("fdb_writer").at("collections");
        } else
#endif
        {
            throw std::runtime_error("Unknown output type: " + output);
        }
    }

    auto grid = ([&]() {
        const auto grid_value = config.at("grid");
        const auto grid_type  = grid_value.at("type").asString();
        if (grid_type == "healpix") {
            const auto grid_order = grid_value.at("order").asString();
            yaco::Grid::HealpixScheme scheme;
            if (grid_order == "ring") {
                scheme = yaco::Grid::HealpixScheme::ring;
            } else if (grid_order == "nested") {
                scheme = yaco::Grid::HealpixScheme::nested;
            } else {
                throw std::runtime_error("Unknown grid order: " + grid_order);
            }
            return yaco::Grid::createHealpix(
                    grid_value.at("name").asString(),
                    grid_value.at("nside").asInt32(),
                    scheme);
        } else {
            throw std::runtime_error("Unknown grid type: " + grid_type);
        }
    })();

    yaco::chrono::setCalendar(yaco::chrono::Calendar::gregorian);
    for (auto collection_config = collections_config.mapBegin();
         collection_config != collections_config.mapEnd();
         ++collection_config) {
        const auto& collection_name = collection_config->first;
        const auto collection_size  = config.at("test_collection_size").at(collection_name).asInt32();
        yaco::Collection collection(collection_name, collection_size, *grid);
        yaco::chrono::DateTime timestep_begin("2000-01-01T00:00:00Z");
        for (int timestep = 0; timestep < 10; ++timestep) {
            for (std::size_t lev = 0; lev < collection_size; ++lev) {
                for (std::size_t i = 0; i < grid->nbrCells(); ++i) {
                    if (lev == 0 && i == 0) {
                        collection.at(lev).at(i) = std::numeric_limits<double>::quiet_NaN();
                    } else {
                        collection.at(lev).at(i) = lev * grid->nbrCells() + i;
                    }
                }
            }
            auto timestep_end = timestep_begin + yaco::chrono::Duration("PT1H");
            data_handler->handle(collection, timestep_begin, timestep_end);
            timestep_begin = timestep_end;
        }
    }
}
