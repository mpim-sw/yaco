// SPDX-FileCopyrightText: 2023 Max Planck Institute for Meteorology, Yaco authors
//
// SPDX-License-Identifier: BSD-3-Clause

#include "yac2_input.hpp"

#include "yac2_grid_definer.hpp"

#include <cmath>
#include <mpi.h>
#include <set>
#include <stdexcept>
#include <vector>
#include <yaco/chrono.hpp>
#include <yaco/collection.hpp>
#include <yaco/config.hpp>

extern "C" {
#include <yac_config.h>
#include <yac.h>
}

namespace yaco
{

std::unique_ptr<YacComponent>
YacComponent::createYac2Input(const config::Value& doc, std::unique_ptr<Grid> grid, std::unique_ptr<DataHandler> handler)
{
    return std::make_unique<Yac2Input>(doc, std::move(grid), std::move(handler));
}

Yac2Input::Yac2Input(const config::Value& config, std::unique_ptr<Grid> grid, std::unique_ptr<DataHandler> handler)
        : name_(config.at("name").asString()), grid_(std::move(grid)), handler_(std::move(handler))
{
    yac_cinit();
    yac_cdef_calendar(YAC_PROLEPTIC_GREGORIAN);
    yac_cread_config_yaml(config.at("yac_yaml").asString().c_str());
    yac_cdef_comp(name_.c_str(), &id_);

    for (const auto& collection : config.at("collections").asArray()) {
        collections_.emplace_back(collection.at("name").asString(), static_cast<std::size_t>(collection.at("size").asInt32()), *grid_);
    }
    timeStep_  = config.at("time_step").asString();
    timeLag_   = config.has("time_lag") ? config.at("time_lag").asInt32() : 0;
    startDate_ = config.at("start_date").asString();
    endDate_   = config.at("end_date").asString();
}

Yac2Input::~Yac2Input()
{
    yac_cfinalize();
}

void Yac2Input::run()
{
    Yac2GridDefiner definer;
    int gridId = grid_->accept(definer);

    std::vector<int> collectionId(collections_.size(), 0);
    for (std::size_t i = 0; i < collections_.size(); ++i) {
        yac_cdef_field(collections_[i].name().c_str(),
                       id_,
                       &gridId,
                       1,
                       static_cast<int>(collections_[i].size()),
                       timeStep_.c_str(),
                       YAC_TIME_UNIT_ISO_FORMAT,
                       &collectionId[i]);
    }

    // start coupling
    yac_cenddef();

    chrono::DateTime timestep_end(startDate_);
    chrono::DateTime end_time(endDate_);
    std::vector<chrono::DateTime> timestep_begin(collections_.size(), timestep_end);

    {
        chrono::Duration lag_duration(timeStep_);
        int timelag = timeLag_;
        if (timelag < 0) {
            timelag = -timelag;
            lag_duration.invert();
        }
        for (int i = 0; i < timelag; ++i) {
            timestep_end += lag_duration;
        }
    }

    chrono::Duration timestep_duration(timeStep_);
    for (; timestep_end <= end_time; timestep_end += timestep_duration) {
        for (std::size_t collectionIdx = 0; collectionIdx < collections_.size(); ++collectionIdx) {
            std::vector<double*> buffers(collections_[collectionIdx].size(), nullptr);
            for (std::size_t fieldIdx = 0; fieldIdx < collections_[collectionIdx].size(); ++fieldIdx) {
                buffers[fieldIdx] = collections_[collectionIdx].at(fieldIdx).data();
            }

            int info   = 0;
            int ierror = 0;
            yac_cget(collectionId[collectionIdx], static_cast<int>(collections_[collectionIdx].size()), buffers.data(), &info, &ierror);

            if (ierror != 0) {
                throw std::runtime_error("Error " + std::to_string(ierror) + " in yac_cget");
            }

            if (info != YAC_ACTION_NONE) {
                handler_->handle(collections_[collectionIdx], timestep_begin[collectionIdx], timestep_end);
                timestep_begin[collectionIdx] = timestep_end;
            }
        }
    }
}

} // namespace yaco
