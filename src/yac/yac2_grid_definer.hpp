// SPDX-FileCopyrightText: 2023 Max Planck Institute for Meteorology, Yaco authors
//
// SPDX-License-Identifier: BSD-3-Clause

#include <vector>
#include <yaco/grid.hpp>

namespace yaco
{

class Yac2GridDefiner final : public GridVisitor {
  public:
    Yac2GridDefiner()  = default;
    ~Yac2GridDefiner() = default;

    int
    visit(UnstructuredGrid& grid) override;

  private:
    std::vector<int> globalIndex_;
    std::vector<int> cellCoreMask_;
};

} // namespace yaco
