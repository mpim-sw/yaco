// SPDX-FileCopyrightText: 2023 Max Planck Institute for Meteorology, Yaco authors
//
// SPDX-License-Identifier: BSD-3-Clause

#include <yaco/grid.hpp>

namespace yaco
{

int UnstructuredGrid::accept(GridVisitor& visitor)
{
    return visitor.visit(*this);
}

} // namespace yaco
