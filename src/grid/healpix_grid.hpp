// SPDX-FileCopyrightText: 2023 Max Planck Institute for Meteorology, Yaco authors
//
// SPDX-License-Identifier: BSD-3-Clause

#ifndef YACO_HEALPIX_GRID_HPP
#define YACO_HEALPIX_GRID_HPP

#include <yaco/grid.hpp>

namespace yaco
{

class HealpixGrid : public UnstructuredGrid {
  public:
    HealpixGrid(std::string name, std::size_t sides, HealpixScheme scheme = HealpixScheme::ring);
    ~HealpixGrid() = default;

    int nbrCells() const noexcept override { return nbrCells_; }
    int nbrVertices() const noexcept override { return nbrVertices_; }
    int* nbrVerticesPerCell() noexcept override { return nbrVerticesPerCell_.data(); }
    int* cellToVertex() noexcept override { return cellToVertex_.data(); }
    double* verticesLon() noexcept override { return verticesLon_.data(); }
    double* verticesLat() noexcept override { return verticesLat_.data(); }
    double* centersLon() noexcept override { return centersLon_.data(); }
    double* centersLat() noexcept override { return centersLat_.data(); }
    const std::string& name() const noexcept override { return name_; }
    std::string get_property(const std::string& key) const override;

  private:
    std::string name_;
    int nbrCells_;
    int nbrVertices_;
    std::size_t sides_;
    HealpixScheme scheme_;
    std::vector<int> nbrVerticesPerCell_;
    std::vector<int> cellToVertex_;
    std::vector<double> verticesLon_;
    std::vector<double> verticesLat_;
    std::vector<double> centersLon_;
    std::vector<double> centersLat_;
};

} // namespace yaco

#endif // YACO_HEALPIX_GRID_HPP
