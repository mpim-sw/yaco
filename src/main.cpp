// SPDX-FileCopyrightText: 2023 Max Planck Institute for Meteorology, Yaco authors
//
// SPDX-License-Identifier: BSD-3-Clause

#include <fstream>
#include <iostream>
#include <memory>
#include <mpi.h>
#include <yaco/grid.hpp>
#include <yaco/yaco.hpp>

int main(int argc, char** argv)
{
    if (argc != 2) {
        std::cerr << "Usage: " << argv[0] << " <config_file>" << std::endl;
        return 1;
    }
    std::string config_filename = argv[1];
    std::ostream& os            = std::cout;

    int size = 0;
    int rank = 0;
    MPI_Init(0, NULL);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    os << "Global Rank " << rank << " of " << size << std::endl;

    yaco::config::Value config = yaco::config::Document::fromYaml(config_filename).value();

    auto data_handler = ([&]() {
        const auto output = config.at("output").asString();
#ifdef YACO_ENABLE_NETCDF
        if (output == "netcdf") {
            return yaco::DataHandler::createNetCDFWriter(config.at("netcdf_writer"));
        } else
#endif
#ifdef YACO_ENABLE_FDB
                if (output == "fdb") {
            return yaco::DataHandler::createFDBWriter(config.at("fdb_writer"));
        } else
#endif
        {
            throw std::runtime_error("Unknown output type: " + output);
        }
    })();

    auto grid = ([&]() {
        const auto grid_value = config.at("grid");
        const auto grid_type  = grid_value.at("type").asString();
        if (grid_type == "healpix") {
            const auto grid_order = grid_value.at("order").asString();
            yaco::Grid::HealpixScheme scheme;
            if (grid_order == "ring") {
                scheme = yaco::Grid::HealpixScheme::ring;
            } else if (grid_order == "nested") {
                scheme = yaco::Grid::HealpixScheme::nested;
            } else {
                throw std::runtime_error("Unknown grid order: " + grid_order);
            }
            return yaco::Grid::createHealpix(
                    grid_value.at("name").asString(),
                    grid_value.at("nside").asInt32(),
                    scheme);
        } else {
            throw std::runtime_error("Unknown grid type: " + grid_type);
        }
    })();

    auto yac_input = config.at("yac_input");
    if (yac_input.has("ranks")) {
        yac_input = yac_input.at("ranks").at(std::to_string(rank));
    }

    yaco::YacComponent::createYac2Input(
            yac_input,
            std::move(grid),
            yaco::DataHandler::createDataLogger(
                    config.has("output_values") && config.at("output_values").asBool(),
                    os,
                    std::move(data_handler)))
            ->run();

    os << "Done" << std::endl;
    MPI_Finalize();
}
