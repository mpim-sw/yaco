// SPDX-FileCopyrightText: 2023 Max Planck Institute for Meteorology, Yaco authors
//
// SPDX-License-Identifier: BSD-3-Clause

#include <cassert>
#include <yaco/chrono.hpp>

extern "C" {
#include <mtime_datetime.h>
#include <mtime_julianDay.h>
}

namespace yaco::chrono
{

void setCalendar(Calendar calendar) noexcept
{
    switch (calendar) {
        case Calendar::gregorian:
            initCalendar(PROLEPTIC_GREGORIAN);
            break;
        case Calendar::_360day:
            initCalendar(YEAR_OF_360_DAYS);
            break;
        case Calendar::_365day:
            initCalendar(YEAR_OF_365_DAYS);
            break;
        default:
            assert(false && "Invalid calendar");
    }
}

} // namespace yaco::Chrono
