// SPDX-FileCopyrightText: 2023 Max Planck Institute for Meteorology, Yaco authors
//
// SPDX-License-Identifier: BSD-3-Clause

#include <libfyaml.h>
#include <yaco/config.hpp>

namespace yaco::config
{

static Value createValueFromYamlNode(struct fy_node* node, std::shared_ptr<Value::Path> path)
{
    fy_node_type type = fy_node_get_type(node);
    switch (type) {
        case FYNT_SCALAR:
            return Value(std::string(fy_node_get_scalar0(node)), path);
        case FYNT_SEQUENCE: {
            Value::Array vec;
            void* iter        = nullptr;
            std::size_t index = 0;
            for (fy_node* child = fy_node_sequence_iterate(node, &iter); child;
                 child          = fy_node_sequence_iterate(node, &iter)) {
                vec.push_back(createValueFromYamlNode(child, std::make_shared<Value::Path>(Value::Path{ "", index, path })));
                ++index;
            }
            return Value(vec, path);
        }
        case FYNT_MAPPING: {
            Value::Map map;
            void* iter = nullptr;
            for (struct fy_node_pair* pair = fy_node_mapping_iterate(node, &iter); pair;
                 pair                      = fy_node_mapping_iterate(node, &iter)) {
                struct fy_node* key = fy_node_pair_key(pair);
                struct fy_node* val = fy_node_pair_value(pair);
                const char* key_str = fy_node_get_scalar0(key);
                map[key_str]        = createValueFromYamlNode(val, std::make_shared<Value::Path>(Value::Path{ key_str, 0, path }));
            }
            return Value(map, path);
        }
        default:
            throw std::runtime_error("Invalid node type");
    }
}

Document Document::fromYaml(const std::string& yml)
{
    Document doc;
    struct fy_document* fyd = fy_document_build_from_file(nullptr, yml.c_str());
    if (!fyd) {
        throw std::runtime_error("Failed to parse YAML");
    }
    if (fy_document_resolve(fyd)) {
        throw std::runtime_error("Failed to resolve YAML anchors");
    }
    doc.value_ = createValueFromYamlNode(fy_document_root(fyd), nullptr);
    return doc;
}

} // namespace yaco::config
