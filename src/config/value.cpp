// SPDX-FileCopyrightText: 2023 Max Planck Institute for Meteorology, Yaco authors
//
// SPDX-License-Identifier: BSD-3-Clause

#include <iostream>
#include <memory>
#include <yaco/config.hpp>

namespace yaco::config
{

Value::Value(std::string value, std::shared_ptr<Value::Path> path)
        : value_(std::move(value)), path_(std::move(path))
{
}

Value::Value(Value::Map value, std::shared_ptr<Value::Path> path)
        : value_(std::move(value)), path_(std::move(path))
{
}

Value::Value(Value::Array value, std::shared_ptr<Value::Path> path)
        : value_(std::move(value)), path_(std::move(path))
{
}

const std::string& Value::asString() const
{
    try {
        return std::any_cast<const std::string&>(value_);
    } catch (const std::bad_any_cast&) {
        throw std::runtime_error("Value is not a scalar: " + path());
    }
}

const std::map<std::string, Value>& Value::asMap() const
{
    try {
        return std::any_cast<const std::map<std::string, Value>&>(value_);
    } catch (const std::bad_any_cast&) {
        throw std::runtime_error("Value is not a map: " + path());
    }
}

const std::vector<Value>& Value::asArray() const
{
    try {
        return std::any_cast<const std::vector<Value>&>(value_);
    } catch (const std::bad_any_cast&) {
        throw std::runtime_error("Value is not a sequence: " + path());
    }
}

std::int32_t Value::asInt32() const
{
    try {
        return std::stoi(asString());
    } catch (const std::invalid_argument&) {
        throw std::runtime_error("Value is not an integer: " + path());
    }
}

double Value::asDouble() const
{
    try {
        return std::stod(asString());
    } catch (const std::invalid_argument&) {
        throw std::runtime_error("Value is not a float: " + path());
    }
}

float Value::asFloat() const
{
    try {
        return std::stof(asString());
    } catch (const std::invalid_argument&) {
        throw std::runtime_error("Value is not a float: " + path());
    }
}

bool Value::asBool() const
{
    const std::string& str = asString();
    if (str == "true") {
        return true;
    } else if (str == "false") {
        return false;
    } else {
        throw std::runtime_error("Value is not a boolean: " + path());
    }
}

bool Value::isMap() const noexcept
{
    return std::any_cast<std::map<std::string, Value>>(&value_) != nullptr;
}

bool Value::isArray() const noexcept
{
    return std::any_cast<std::vector<Value>>(&value_) != nullptr;
}

bool Value::isScalar() const noexcept
{
    return std::any_cast<std::string>(&value_) != nullptr;
}

void Value::set(const std::string& value)
{
    value_ = value;
}

void Value::set(const std::map<std::string, Value>& value)
{
    value_ = value;
}

void Value::set(const std::vector<Value>& value)
{
    value_ = value;
}

void Value::set(std::int32_t value)
{
    value_ = std::to_string(value);
}

void Value::set(bool value)
{
    value_ = value ? "true" : "false";
}

Value& Value::operator[](const std::string& key) noexcept
{
    try {
        return (*std::any_cast<std::map<std::string, Value>>(&value_))[key];
    } catch (const std::bad_any_cast&) {
        std::cerr << "Value is not a map: " << path() << std::endl;
        std::terminate();
    }
}

Value& Value::at(const std::string& key)
{
    auto it = find(key);
    if (it == mapEnd()) {
        throw std::out_of_range("Key '" + key + "' not found in " + path());
    }
    return it->second;
}

const Value& Value::at(const std::string& key) const
{
    auto it = find(key);
    if (it == mapEnd()) {
        throw std::out_of_range("Key '" + key + "' not found in " + path());
    }
    return it->second;
}

Value::MapIterator Value::mapBegin()
{
    try {
        return std::any_cast<std::map<std::string, Value>&>(value_).begin();
    } catch (const std::bad_any_cast&) {
        throw std::runtime_error("Value is not a map: " + path());
    }
}

Value::MapIterator Value::mapEnd()
{
    try {
        return std::any_cast<std::map<std::string, Value>&>(value_).end();
    } catch (const std::bad_any_cast&) {
        throw std::runtime_error("Value is not a map: " + path());
    }
}

Value::ConstMapIterator Value::mapBegin() const
{
    try {
        return std::any_cast<const std::map<std::string, Value>&>(value_).begin();
    } catch (const std::bad_any_cast&) {
        throw std::runtime_error("Value is not a map: " + path());
    }
}

Value::ConstMapIterator Value::mapEnd() const
{
    try {
        return std::any_cast<const std::map<std::string, Value>&>(value_).end();
    } catch (const std::bad_any_cast&) {
        throw std::runtime_error("Value is not a map: " + path());
    }
}

Value::MapIterator Value::find(const std::string& key)
{
    try {
        return std::any_cast<std::map<std::string, Value>&>(value_).find(key);
    } catch (const std::bad_any_cast&) {
        throw std::runtime_error("Value is not a map: " + path());
    }
}

Value::ConstMapIterator Value::find(const std::string& key) const
{
    try {
        return std::any_cast<const std::map<std::string, Value>&>(value_).find(key);
    } catch (const std::bad_any_cast&) {
        throw std::runtime_error("Value is not a map: " + path());
    }
}

bool Value::has(const std::string& key) const
{
    return find(key) != mapEnd();
}

Value& Value::operator[](std::size_t i) noexcept
{
    try {
        return std::any_cast<std::vector<Value>&>(value_)[i];
    } catch (const std::bad_any_cast&) {
        std::cerr << "Value is not a sequence: " << path() << std::endl;
        std::terminate();
    }
}

const Value& Value::operator[](std::size_t i) const noexcept
{
    try {
        return std::any_cast<const std::vector<Value>&>(value_)[i];
    } catch (const std::bad_any_cast&) {
        std::cerr << "Value is not a sequence: " << path() << std::endl;
        std::terminate();
    }
}

Value& Value::at(std::size_t i)
{
    try {
        return std::any_cast<std::vector<Value>&>(value_).at(i);
    } catch (const std::bad_any_cast&) {
        throw std::runtime_error("Value is not a sequence: " + path());
    }
}

const Value& Value::at(std::size_t i) const
{
    try {
        return std::any_cast<const std::vector<Value>&>(value_).at(i);
    } catch (const std::bad_any_cast&) {
        throw std::runtime_error("Value is not a sequence: " + path());
    }
}

Value::ArrayIterator Value::arrayBegin()
{
    try {
        return std::any_cast<std::vector<Value>&>(value_).begin();
    } catch (const std::bad_any_cast&) {
        throw std::runtime_error("Value is not a sequence: " + path());
    }
}

Value::ArrayIterator Value::arrayEnd()
{
    try {
        return std::any_cast<std::vector<Value>&>(value_).end();
    } catch (const std::bad_any_cast&) {
        throw std::runtime_error("Value is not a sequence: " + path());
    }
}

Value::ConstArrayIterator Value::arrayBegin() const
{
    try {
        return std::any_cast<const std::vector<Value>&>(value_).begin();
    } catch (const std::bad_any_cast&) {
        throw std::runtime_error("Value is not a sequence: " + path());
    }
}

Value::ConstArrayIterator Value::arrayEnd() const
{
    try {
        return std::any_cast<const std::vector<Value>&>(value_).end();
    } catch (const std::bad_any_cast&) {
        throw std::runtime_error("Value is not a sequence: " + path());
    }
}

std::string Value::path() const
{
    std::string res = "";
    Path* p         = path_ ? path_.get() : nullptr;
    while (p != nullptr) {
        if (!res.empty()) {
            res = "/" + res;
        }
        if (p->name.empty()) {
            res = "[" + std::to_string(p->index) + "]" + res;
        } else {
            res = p->name + res;
        }
        p = p->parent ? p->parent.get() : nullptr;
    }
    return "/" + res;
}

} // namespace yaco::config
