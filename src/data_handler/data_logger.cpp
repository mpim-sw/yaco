// SPDX-FileCopyrightText: 2023 Max Planck Institute for Meteorology, Yaco authors
//
// SPDX-License-Identifier: BSD-3-Clause

#include "data_logger.hpp"

#include <yaco/chrono.hpp>

namespace yaco
{

std::unique_ptr<DataHandler>
DataHandler::createDataLogger(bool output_values, std::ostream& os, std::unique_ptr<DataHandler> handler)
{
    return std::make_unique<DataLogger>(output_values, os, std::move(handler));
}

DataLogger::DataLogger(bool output_values, std::ostream& os, std::unique_ptr<DataHandler> handler)
        : output_values_(output_values), os_(os), handler_(std::move(handler))
{
}

void DataLogger::handle(Collection& collection, const chrono::DateTime& timestep_begin, const chrono::DateTime& timestep_end)
{
    os_ << "Name: " << collection.name() << std::endl;
    os_ << "Time (step begin): " << timestep_begin.toString("%Y %b %d %H:%M:%S") << std::endl;
    os_ << "Time (step end): " << timestep_end.toString("%Y %b %d %H:%M:%S") << std::endl;
    if (output_values_) {
        for (std::size_t i = 0; i < collection.size(); ++i) {
            for (auto value : collection.at(i)) {
                os_ << value << " ";
            }
            os_ << std::endl;
        }
        os_ << "-----------------------------------" << std::endl;
    }

    if (handler_) {
        handler_->handle(collection, timestep_begin, timestep_end);
    }
    os_ << "Finished writing : " << collection.name() << std::endl;
}

} // namespace yaco
