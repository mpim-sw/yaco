// SPDX-FileCopyrightText: 2023 Max Planck Institute for Meteorology, Yaco authors
//
// SPDX-License-Identifier: BSD-3-Clause

#ifndef YACO_FDB_WRITER_HPP
#define YACO_FDB_WRITER_HPP

#include <fstream>
#include <memory>
#include <yaco/data_handler.hpp>

namespace yaco
{

class FDBWriter : public DataHandler {
  public:
    FDBWriter(config::Value config);
    ~FDBWriter() override;
    void handle(Collection& collection, const chrono::DateTime& timestep_begin, const chrono::DateTime& timestep_end) override;

  private:
    class Impl;
    std::unique_ptr<Impl> impl_;
};

} // namespace yaco

#endif // YACO_FDB_WRITER_HPP
