// SPDX-FileCopyrightText: 2023 Max Planck Institute for Meteorology, Yaco authors
//
// SPDX-License-Identifier: BSD-3-Clause

#ifndef YACO_DATA_LOGGER_HPP
#define YACO_DATA_LOGGER_HPP

#include <fstream>
#include <memory>
#include <yaco/data_handler.hpp>

namespace yaco
{

class DataLogger : public DataHandler {
  public:
    DataLogger(bool output_values, std::ostream& os, std::unique_ptr<DataHandler> handler = nullptr);
    ~DataLogger() override = default;
    void handle(Collection& field, const chrono::DateTime& timestep_begin, const chrono::DateTime& timestep_end) override;

  private:
    bool output_values_;
    std::ostream& os_;
    std::unique_ptr<DataHandler> handler_;
};

} // namespace yaco

#endif // YACO_DATA_LOGGER_HPP
