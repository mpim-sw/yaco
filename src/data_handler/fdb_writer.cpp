// SPDX-FileCopyrightText: 2023 Max Planck Institute for Meteorology, Yaco authors
//
// SPDX-License-Identifier: BSD-3-Clause

#include "fdb_writer.hpp"

#include <eccodes.h>
#include <eckit/runtime/Main.h>
#include <fdb5/api/FDB.h>
#include <fdb5/config/Config.h>
#include <yaco/chrono.hpp>
#include <yaco/collection.hpp>
#include <yaco/config.hpp>
#include <yaco/grid.hpp>

namespace yaco
{

std::unique_ptr<DataHandler> DataHandler::createFDBWriter(config::Value config)
{
    return std::make_unique<FDBWriter>(std::move(config));
}

static std::vector<std::uint8_t> bytesFromHexString(const std::string& hex)
{
    std::vector<std::uint8_t> bytes;
    for (std::size_t i = 0; i < hex.size(); i += 2) {
        bytes.push_back(static_cast<std::uint8_t>(std::stoi(hex.substr(i, 2), nullptr, 16)));
    }
    return bytes;
}

static std::string hexStringFromBytes(const std::vector<std::uint8_t>& bytes)
{
    std::string str(bytes.size() * 2, ' ');
    for (std::size_t i = 0; i < bytes.size(); ++i) {
        sprintf(&str[2 * i], "%02x", bytes[i]);
    }
    return str;
}

class GRIBMessage {
  private:
    codes_handle* handle_ = nullptr;

  public:
    GRIBMessage()
    {
        handle_ = codes_grib_handle_new_from_samples(0, "GRIB2");
        if (handle_ == nullptr) {
            throw std::runtime_error("Failed to create eccodes handle");
        }
    }
    ~GRIBMessage()
    {
        codes_handle_delete(handle_);
    }

    void set(const std::string& key, const Field& field)
    {
        CODES_CHECK(codes_set_double_array(handle_, key.c_str(), field.data(), field.size()), 0);
    }

    void set(const std::string& key, const std::string& value)
    {
        char* end;

        {
            long val = std::strtol(value.c_str(), &end, 10);
            if (*end == '\0') {
                set(key, val);
                return;
            }
        }

        {
            double val = std::strtod(value.c_str(), &end);
            if (*end == '\0') {
                set(key, val);
                return;
            }
        }

        set_string(key, value);
    }

    void set_string(const std::string& key, const std::string& value)
    {
        std::size_t len = value.size();
        int res         = codes_set_string(handle_, key.c_str(), value.c_str(), &len);
        if (res != 0) {
            throw std::runtime_error("GRIB: Failed to set key '" + key + "' to string '" + value + "': " + codes_get_error_message(res));
        }
    }

    void set(const std::string& key, const std::vector<std::uint8_t>& value)
    {
        std::size_t len = value.size();
        int res         = codes_set_bytes(handle_, key.c_str(), (const unsigned char*)&value[0], &len);
        if (res != 0) {
            throw std::runtime_error("GRIB: Failed to set key '" + key + "' to string '" + hexStringFromBytes(value) + "': " + codes_get_error_message(res));
        }
    }

    void set(const std::string& key, double value)
    {
        int res = codes_set_double(handle_, key.c_str(), value);
        if (res != 0) {
            throw std::runtime_error("GRIB: Failed to set key '" + key + "' to double '" + std::to_string(value) + "': " + codes_get_error_message(res));
        }
    }

    void set(const std::string& key, long value)
    {
        int res = codes_set_long(handle_, key.c_str(), value);
        if (res != 0) {
            throw std::runtime_error("GRIB: Failed to set key '" + key + "' to long '" + std::to_string(value) + "': " + codes_get_error_message(res));
        }
    }

    std::string get(const std::string& key, bool maybeempty)
    {
        constexpr auto MAX_VALUE_LEN = 1024;
        char value[MAX_VALUE_LEN];
        std::size_t len = MAX_VALUE_LEN - 1;
        int res         = codes_get_string(handle_, key.c_str(), value, &len);
        if (res != 0) {
            if (maybeempty && res == CODES_NOT_FOUND) {
                return "";
            }
            throw std::runtime_error("GRIB: Failed to get key '" + key + "': " + codes_get_error_message(res));
        }
        value[len] = '\0';
        return value;
    }

    void archive(fdb5::FDB& fdb, const fdb5::Key& key)
    {
        const void* data;
        std::size_t data_length;
        int res = codes_get_message(handle_, &data, &data_length);
        if (res != 0) {
            throw std::runtime_error("GRIB: Failed to get raw message: " + std::string(codes_get_error_message(res)));
        }
        fdb.archive(key, data, data_length);
    }
};

class MetadataHandler {
  public:
    using HandleFunction = std::function<std::string(const config::Value&)>;

    MetadataHandler()  = default;
    ~MetadataHandler() = default;

    void bind(const std::string& param, HandleFunction handler)
    {
        handlers_[param] = std::move(handler);
    }

    template<class T>
    void process_source(const std::string& parent_key, const config::Value& parent_section, T& target_dict, const Field& field)
    {

        const auto& source = parent_section.at("source").asString();
        if (source == "yaco") {
            const auto& key = parent_section.at("key").asString();
            auto value      = handlers_.at(key)(parent_section);
            if (!value.empty()) {
                target_dict.set(parent_key, std::move(value));
            }
        } else if (source == "grib") {
            auto value = handlers_.at("grib")(parent_section);
            if (!value.empty()) {
                target_dict.set(parent_key, std::move(value));
            }
        } else if (source == "data") {
            if constexpr (std::is_same_v<T, GRIBMessage>) {
                target_dict.set(parent_key, field);
            } else {
                throw std::runtime_error("'Source'='data' can only be used in grib_metadata " + parent_section.at("data").path());
            }
        } else if (source == "grid") {
            auto value = handlers_.at("grid")(parent_section);
            if (!value.empty()) {
                target_dict.set(parent_key, std::move(value));
            }
        } else {
            throw std::runtime_error("Source must be 'yaco', 'grib','data', or 'grid' for " + parent_section.at("source").path());
        }
    }

    // 'only_in_case_of_data' is a dirty hack and should be removed by fixing the handlers to not only return strings
    template<class T>
    void configToParamDict(const config::Value& config, T& target_dict, std::vector<double>& only_in_case_of_data)
    {
        static_assert(std::is_same_v<T, GRIBMessage> || std::is_same_v<T, fdb5::Key>);

        for (auto array_iter = config.arrayBegin(); array_iter != config.arrayEnd(); ++array_iter) {
            for (auto iter = array_iter->mapBegin(); iter != array_iter->mapEnd(); ++iter) {
                if (iter->second.isScalar()) {
                    target_dict.set(iter->first, iter->second.asString());
                } else {

                    if (!iter->second.isMap()) {
                        throw std::runtime_error("Expected scalar or map value for " + iter->second.path());
                    }

                    if (iter->second.has("source")) {

                        process_source(iter->first, iter->second, target_dict, only_in_case_of_data);

                    } else if (iter->second.has("value")) {

                        const auto& value   = iter->second.at("value").asString();
                        const auto& convert = iter->second.at("convert").asString();
                        if (convert == "hex_to_bytes") {
                            if constexpr (std::is_same_v<T, GRIBMessage>) {
                                target_dict.set(iter->first, bytesFromHexString(value));
                            } else {
                                target_dict.set(iter->first, value);
                            }
                        } else {
                            throw std::runtime_error("Unknown conversion '" + convert + "' in " + iter->second.at("convert").path());
                        }

                    } else {
                        throw std::runtime_error("Expected keys 'source' or 'value' in " + iter->second.path());
                    }
                }
            }
        }
    }

  private:
    std::map<std::string, HandleFunction> handlers_;
};

class FDBWriter::Impl {
  public:
    Impl(config::Value config) : config_(std::move(config))
    {
        if (!fdb_lib_initialized_) {
            std::string arg = "fdb";
            std::array<char*, 2> argv({ arg.data(), nullptr });
            eckit::Main::initialise(argv.size() - 1, argv.data());
            fdb_lib_initialized_ = true;
        }

        const auto fdb_config = config_.find("fdb_config");
        if (fdb_config != config_.mapEnd()) {
            fdb_ = std::make_unique<fdb5::FDB>(fdb5::Config::make(fdb_config->second.asString()));
        } else {
            fdb_ = std::make_unique<fdb5::FDB>();
        }
    }

    void handle(Collection& collection, const chrono::DateTime& timestep_begin, const chrono::DateTime& timestep_end);

  private:
    static bool fdb_lib_initialized_;
    config::Value config_;
    std::unique_ptr<fdb5::FDB> fdb_;
};

bool FDBWriter::Impl::fdb_lib_initialized_ = false;

FDBWriter::FDBWriter(config::Value config)
        : impl_(std::make_unique<Impl>(std::move(config)))
{
}

FDBWriter::~FDBWriter() = default;

void FDBWriter::Impl::handle(Collection& collection, const chrono::DateTime& timestep_begin, const chrono::DateTime& timestep_end)
{
    const auto& collectionNode = config_.at("collections").at(collection.name());

    GRIBMessage msg;
    std::size_t collection_index;
    double missing_value = -9.e33;

    if (config_.has("missing_value")) {
        missing_value = static_cast<double>(config_.at("missing_value").asFloat());
    }

    MetadataHandler metadata_handler;
    metadata_handler.bind("collection_index", [&collection_index](const config::Value& params) {
        if (params.has("offset")) {
            const auto offset = static_cast<std::size_t>(params.at("offset").asInt32());
            return std::to_string(collection_index + offset);
        }
        if (params.has("lookup")) {
            if (!params.at("lookup_table").isArray()) {
                throw std::runtime_error("Expected array for lookup table in " + params.at("lookup_table").path());
            }
            const auto& lookup_table = params.at("lookup_table").asArray();
            const auto& lookup_type  = params.at("lookup").asString();
            if (lookup_type == "direct") {
                if (collection_index >= lookup_table.size()) {
                    throw std::runtime_error("Collection index " + std::to_string(collection_index) + " out of range for lookup table " + params.at("lookup_table").path());
                }
                return lookup_table[collection_index].asString();
            }
            if (lookup_type == "accumulate") {
                if (collection_index >= lookup_table.size() + 1) {
                    throw std::runtime_error("Collection index " + std::to_string(collection_index) + " out of range for lookup table " + params.at("lookup_table").path());
                }
                double sum = 0;
                for (std::size_t i = 0; i < collection_index; ++i) {
                    sum += lookup_table[i].asDouble();
                }
                return std::to_string(sum);
            }
            if (lookup_type == "accumulate_half") {
                if (collection_index >= lookup_table.size()) {
                    throw std::runtime_error("Collection index " + std::to_string(collection_index) + " out of range for lookup table " + params.at("lookup_table").path());
                }
                double sum = 0;
                for (std::size_t i = 1; i < collection_index; ++i) {
                    sum += lookup_table[i - 1].asDouble();
                }
                return std::to_string(sum + lookup_table[collection_index].asDouble() / 2.);
            }
            throw std::runtime_error("Unknown lookup type '" + lookup_type + "' in " + params.at("lookup").path());
        }
        return std::to_string(collection_index);
    });
    metadata_handler.bind("timestep_begin", [&timestep_begin](const config::Value& params) {
        return timestep_begin.toString(params.at("format").asString().c_str());
    });
    metadata_handler.bind("timestep_end", [&timestep_end](const config::Value& params) {
        return timestep_end.toString(params.at("format").asString().c_str());
    });
    metadata_handler.bind("grib", [&msg](const config::Value& params) {
        return msg.get(params.at("key").asString(), params.has("maybeempty") && params.at("maybeempty").asBool());
    });
    const auto& grid = collection.grid();
    metadata_handler.bind("grid", [&grid](const config::Value& params) {
        return grid.get_property(params.at("key").asString());
    });
    metadata_handler.bind("missing_value", [&missing_value](const config::Value&) {
        return std::to_string(missing_value);
    });
    bool has_missing_values = false;
    metadata_handler.bind("has_missing_values", [&has_missing_values](const config::Value&) {
        return has_missing_values ? "1" : "0";
    });

    for (collection_index = 0; collection_index < collection.size(); ++collection_index) {
        auto& values = collection.at(collection_index);

        // Set NANs to missing value
        has_missing_values = false;
        for (double& value : values) {
            if (std::isnan(value)) {
                value              = missing_value;
                has_missing_values = true;
            }
        }

        if (config_.has("grib_metadata")) {
            metadata_handler.configToParamDict(config_.at("grib_metadata"), msg, values);
        }
        if (collectionNode.has("grib_metadata")) {
            metadata_handler.configToParamDict(collectionNode.at("grib_metadata"), msg, values);
        }

        fdb5::Key key{};
        if (config_.has("fdb_metadata")) {
            metadata_handler.configToParamDict(config_.at("fdb_metadata"), key, values);
        }
        if (collectionNode.has("fdb_metadata")) {
            metadata_handler.configToParamDict(collectionNode.at("fdb_metadata"), key, values);
        }
        msg.archive(*fdb_, key);
    }

    fdb_->flush();
}

void FDBWriter::handle(Collection& collection, const chrono::DateTime& timestep_begin, const chrono::DateTime& timestep_end)
{
    impl_->handle(collection, timestep_begin, timestep_end);
}

} // namespace yaco
