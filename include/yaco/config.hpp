// SPDX-FileCopyrightText: 2023 Max Planck Institute for Meteorology, Yaco authors
//
// SPDX-License-Identifier: BSD-3-Clause

#ifndef YACO_CONFIG_HPP
#define YACO_CONFIG_HPP

#include <any>
#include <cstdint>
#include <map>
#include <memory>
#include <stdexcept>
#include <string>
#include <vector>

namespace yaco::config
{

class Value {
  public:
    using Map                = std::map<std::string, Value>;
    using Array              = std::vector<Value>;
    using MapIterator        = Map::iterator;
    using ConstMapIterator   = Map::const_iterator;
    using ArrayIterator      = Array::iterator;
    using ConstArrayIterator = Array::const_iterator;

    struct Path {
        std::string name;
        std::size_t index;
        std::shared_ptr<Path> parent;
    };

    Value() = default;
    Value(std::string value, std::shared_ptr<Path> path);
    Value(Map value, std::shared_ptr<Path> path);
    Value(Array value, std::shared_ptr<Path> path);

    const std::string& asString() const;
    const Map& asMap() const;
    const Array& asArray() const;
    std::int32_t asInt32() const;
    float asFloat() const;
    double asDouble() const;
    bool asBool() const;

    bool isMap() const noexcept;
    bool isArray() const noexcept;
    bool isScalar() const noexcept;

    void set(const std::string& value);
    void set(const Map& value);
    void set(const Array& value);
    void set(std::int32_t value);
    void set(bool value);

    // access operators for maps
    Value& operator[](const std::string& key) noexcept;

    Value& at(const std::string& key);
    const Value& at(const std::string& key) const;

    MapIterator mapBegin();
    MapIterator mapEnd();

    ConstMapIterator mapBegin() const;
    ConstMapIterator mapEnd() const;

    MapIterator find(const std::string& key);
    ConstMapIterator find(const std::string& key) const;

    bool has(const std::string& key) const;

    // access operators for arrays
    Value& operator[](std::size_t i) noexcept;
    const Value& operator[](std::size_t i) const noexcept;

    Value& at(std::size_t i);
    const Value& at(std::size_t i) const;

    ArrayIterator arrayBegin();
    ArrayIterator arrayEnd();

    ConstArrayIterator arrayBegin() const;
    ConstArrayIterator arrayEnd() const;

    std::string path() const;

  private:
    std::any value_;
    std::shared_ptr<Path> path_;
};

class Document {
  public:
    static Document fromYaml(const std::string& yml);

    Document()          = default;
    virtual ~Document() = default;
    Value& value() { return value_; }
    const Value& value() const { return value_; }

  private:
    Value value_;
};

} // namespace yaco::config

#endif
