// SPDX-FileCopyrightText: 2023 Max Planck Institute for Meteorology, Yaco authors
//
// SPDX-License-Identifier: BSD-3-Clause

#ifndef YACO_GRID_HPP
#define YACO_GRID_HPP

#include <memory>
#include <string>
#include <vector>

namespace yaco
{

class GridVisitor;

class Grid {
  public:
    enum class HealpixScheme {
        ring   = 0,
        nested = 1,
    };

    static std::unique_ptr<Grid> createHealpix(std::string name, std::size_t side, HealpixScheme scheme);

    Grid()          = default;
    virtual ~Grid() = default;

    virtual const std::string& name() const noexcept = 0;

    virtual std::string get_property(const std::string& key) const = 0;

    virtual int accept(GridVisitor& visitor) = 0;

    virtual int
    nbrVertices() const noexcept = 0;

    virtual int
    nbrCells() const noexcept = 0;
};

class UnstructuredGrid : public Grid {
  public:
    UnstructuredGrid()          = default;
    virtual ~UnstructuredGrid() = default;

    virtual int
    accept(GridVisitor& definer) override;

    virtual int*
    nbrVerticesPerCell() noexcept = 0;

    virtual int*
    cellToVertex() noexcept = 0;

    virtual double*
    verticesLon() noexcept = 0;

    virtual double*
    verticesLat() noexcept = 0;

    virtual double*
    centersLon() noexcept = 0;

    virtual double*
    centersLat() noexcept = 0;
};

class GridVisitor {
  public:
    GridVisitor()          = default;
    virtual ~GridVisitor() = default;

    virtual int
    visit(UnstructuredGrid& grid) = 0;
};

} // namespace yaco

#endif
