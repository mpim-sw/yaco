// SPDX-FileCopyrightText: 2023 Max Planck Institute for Meteorology, Yaco authors
//
// SPDX-License-Identifier: BSD-3-Clause

#ifndef YACO_CHRONO_HPP
#define YACO_CHRONO_HPP

#include <chrono>
#include <string>

namespace yaco::chrono
{

enum class Calendar {
    gregorian,
    _365day,
    _360day,
};

void setCalendar(Calendar calendar) noexcept;

class DateTime;
class Duration final {
  public:
    struct ModuloResult {
        std::chrono::milliseconds remainder;
        std::int64_t quotient;
    };
    Duration();

    Duration(const std::string& iso8601);

    Duration(const Duration& other);
    Duration& operator=(const Duration& other);

    Duration(Duration&& other) noexcept;
    Duration& operator=(Duration&& other) noexcept;

    ~Duration();

    std::chrono::seconds toSecondsFrom(const DateTime& refTime) const noexcept;
    std::chrono::milliseconds toMillisecondsFrom(const DateTime& refTime) const noexcept;

    std::chrono::seconds toSeconds() const;
    std::chrono::milliseconds toMilliseconds() const noexcept;

    ModuloResult modulo(const Duration& n) const noexcept;
    void invert() noexcept;

    bool operator==(const Duration& other) const noexcept;
    bool operator!=(const Duration& other) const noexcept;
    bool operator<(const Duration& other) const noexcept;
    bool operator>(const Duration& other) const noexcept;
    bool operator<=(const Duration& other) const noexcept;
    bool operator>=(const Duration& other) const noexcept;

  private:
    friend class DateTime;

    void* impl_;
};

class DateTime final {
  public:
    DateTime(const std::string& iso8601);

    DateTime(const DateTime& other);
    DateTime& operator=(const DateTime& other);

    DateTime(DateTime&& other) noexcept;
    DateTime& operator=(DateTime&& other) noexcept;

    ~DateTime();

    int year() const noexcept;
    int month() const noexcept;
    int day() const noexcept;
    int hour() const noexcept;
    int minute() const noexcept;
    int second() const noexcept;

    std::string
    toString(const char* fmt) const noexcept;

    bool operator<(const DateTime& other) const noexcept;
    bool operator>(const DateTime& other) const noexcept;
    bool operator<=(const DateTime& other) const noexcept;
    bool operator>=(const DateTime& other) const noexcept;
    bool operator==(const DateTime& other) const noexcept;
    bool operator!=(const DateTime& other) const noexcept;

    DateTime operator+(const Duration& other) const noexcept;
    DateTime& operator+=(const Duration& other) noexcept;

    Duration operator-(const DateTime& other) const noexcept;

  private:
    friend class Duration;
    void* impl_;
};

} // namespace yaco::chrono

#endif
