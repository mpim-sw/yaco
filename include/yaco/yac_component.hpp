// SPDX-FileCopyrightText: 2023 Max Planck Institute for Meteorology, Yaco authors
//
// SPDX-License-Identifier: BSD-3-Clause

#ifndef YACO_YAC_COMPONENT_HPP
#define YACO_YAC_COMPONENT_HPP

#include "config.hpp"
#include "data_handler.hpp"
#include "grid.hpp"

#include <memory>
#include <string>

namespace yaco
{

class YacComponent {
  public:
    static std::unique_ptr<YacComponent>
    createYac2Input(const config::Value& config,
                    std::unique_ptr<Grid> grid,
                    std::unique_ptr<DataHandler> handler = nullptr);

    YacComponent()          = default;
    virtual ~YacComponent() = default;
    virtual void run()      = 0;
};

} // namespace yaco

#endif // YACO_COMPONENT_HPP
