find_package(PkgConfig REQUIRED)
set(PKG_CONFIG_USE_CMAKE_PREFIX_PATH ON)
pkg_check_modules(YAC_PKGCONFIG REQUIRED yac)

if(YAC_PKGCONFIG_FOUND)
  add_library(YAC::YAC INTERFACE IMPORTED)
  set(YAC_PKGCONFIG_ALL_LIBS ${YAC_PKGCONFIG_LINK_LIBRARIES} ${YAC_PKGCONFIG_LDFLAGS_OTHER})
  set_target_properties(YAC::YAC PROPERTIES
    IMPORTED_LINK_INTERFACE_LANGUAGES "C"
    INTERFACE_INCLUDE_DIRECTORIES "${YAC_PKGCONFIG_INCLUDE_DIRS}"
    INTERFACE_LINK_LIBRARIES "${YAC_PKGCONFIG_ALL_LIBS}"
    INTERFACE_COMPILE_OPTIONS "${YAC_PKGCONFIG_CFLAGS_OTHER}"
  )
  # the pkg-config may state to use mpicc as compiler. Instead we link MPI here explicitly.
  # As we include yac.h (which includes mpi.h) from CXX we need to link against MPI_CXX instead of MPI_C
  target_link_libraries(YAC::YAC INTERFACE MPI::MPI_CXX)
endif()