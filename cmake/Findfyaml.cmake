# SPDX-FileCopyrightText: 2023 Max Planck Institute for Meteorology, Yaco authors
#
# SPDX-License-Identifier: BSD-3-Clause

if(FYAML_INCLUDE_DIR AND FYAML_LIBRARY)
  set(FYAML_FIND_QUIETLY TRUE)
endif()

find_path(FYAML_INCLUDE_DIR libfyaml.h)

find_library(FYAML_LIBRARY fyaml)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(fyaml DEFAULT_MSG FYAML_INCLUDE_DIR FYAML_LIBRARY)

mark_as_advanced(FYAML_INCLUDE_DIR FYAML_LIBRARY)

set(FYAML_LIBRARIES ${FYAML_LIBRARY})
set(FYAML_INCLUDE_DIRS ${FYAML_INCLUDE_DIR})

if(FYAML_FOUND AND NOT TARGET fyaml::fyaml)
  add_library(fyaml::fyaml UNKNOWN IMPORTED)
  set_target_properties(fyaml::fyaml PROPERTIES INTERFACE_INCLUDE_DIRECTORIES "${FYAML_INCLUDE_DIRS}")
  set_target_properties(fyaml::fyaml PROPERTIES IMPORTED_LINK_INTERFACE_LANGUAGES "C")
  set_target_properties(fyaml::fyaml PROPERTIES IMPORTED_LOCATION "${FYAML_LIBRARIES}")

  if (NOT FYAML_FIND_QUIETLY)
    message(STATUS "Found fyaml: ${FYAML_LIBRARIES}")
  endif()
endif()
